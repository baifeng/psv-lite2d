
#include "GameView.h"
#include "lite2d.h"
#include "LandMine.h"
#include "math.h"
#include <vita2d.h>

static unsigned short GRID_SIZE = 54;

// images
extern unsigned char _binary_assets_img_tile_1_png_start;
extern unsigned char _binary_assets_img_tile_2_png_start;
extern unsigned char _binary_assets_img_tile_3_png_start;
extern unsigned char _binary_assets_img_tile_4_png_start;
extern unsigned char _binary_assets_img_tile_5_png_start;
extern unsigned char _binary_assets_img_tile_6_png_start;
extern unsigned char _binary_assets_img_tile_7_png_start;
extern unsigned char _binary_assets_img_tile_8_png_start;
extern unsigned char _binary_assets_img_tile_b_png_start;
extern unsigned char _binary_assets_img_tile_b2_png_start;
extern unsigned char _binary_assets_img_tile_b3_png_start;
extern unsigned char _binary_assets_img_tile_base_png_start;
extern unsigned char _binary_assets_img_tile_d_png_start;
extern unsigned char _binary_assets_img_tile_down_png_start;
extern unsigned char _binary_assets_img_tile_f_png_start;
extern unsigned char _binary_assets_img_tile_mask_png_start;

// texture
lite2d::Texture* texNumber[8] = {NULL}; // 数字
lite2d::Texture* texTileB = NULL;       // 地雷
lite2d::Texture* texTileB2 = NULL;      // 地雷
lite2d::Texture* texTileD = NULL;       // 旗子
lite2d::Texture* texTileF = NULL;       // 标旗提示
lite2d::Texture* texTileBase = NULL;    // 默认块
lite2d::Texture* texTileMask = NULL;    // 遮罩
lite2d::Texture* texTileDown = NULL;    // 按下

// func
void initTexture() {
	unsigned char* number[8] {
		&_binary_assets_img_tile_1_png_start,
		&_binary_assets_img_tile_2_png_start,
		&_binary_assets_img_tile_3_png_start,
		&_binary_assets_img_tile_4_png_start,
		&_binary_assets_img_tile_5_png_start,
		&_binary_assets_img_tile_6_png_start,
		&_binary_assets_img_tile_7_png_start,
		&_binary_assets_img_tile_8_png_start,
	};
	for (int i = 0; i < 8; ++i) {
		vita2d_texture* tex = vita2d_load_PNG_buffer(number[i]);
		texNumber[i] = new lite2d::Texture(tex);
	}
	texTileB = new lite2d::Texture(vita2d_load_PNG_buffer(&_binary_assets_img_tile_b_png_start));
	texTileB2 = new lite2d::Texture(vita2d_load_PNG_buffer(&_binary_assets_img_tile_b2_png_start));
	texTileD = new lite2d::Texture(vita2d_load_PNG_buffer(&_binary_assets_img_tile_d_png_start));
	texTileF = new lite2d::Texture(vita2d_load_PNG_buffer(&_binary_assets_img_tile_f_png_start));
	texTileBase = new lite2d::Texture(vita2d_load_PNG_buffer(&_binary_assets_img_tile_base_png_start));
	texTileMask = new lite2d::Texture(vita2d_load_PNG_buffer(&_binary_assets_img_tile_mask_png_start));
	texTileDown = new lite2d::Texture(vita2d_load_PNG_buffer(&_binary_assets_img_tile_down_png_start));
}

void finiTexture() {
	for (int i = 0; i < 8; ++i) {
		SAFE_RELEASE(texNumber[i]);
	}
	SAFE_RELEASE(texTileB);
	SAFE_RELEASE(texTileB2);
	SAFE_RELEASE(texTileD);
	SAFE_RELEASE(texTileF);
	SAFE_RELEASE(texTileBase);
	SAFE_RELEASE(texTileMask);
	SAFE_RELEASE(texTileDown);
}

void getCoord(int x, int y, int* rx, int* ry) {
	*rx = x / GRID_SIZE;
	*ry = y / GRID_SIZE;
}

GameView::GameView() {
	this->miOffsetX = 0;
	this->miOffsetY = 0;
	this->mbFlagMode = false;
	this->mbCanMoveX = false;
	this->mbCanMoveY = false;
	this->mpTileMap = &(TileMapProxy&)lite2d::getProxy(TileMapProxy::ProxyName);

	::initTexture();

	this->setCurTileNull();
}

GameView::~GameView() {
	::finiTexture();
}

void GameView::initGame() {
	TileMapProxy& tileMap = *this->mpTileMap;

	GRID_SIZE = 64;

	//TileMapProxy& tileMap = *this->mpTileMap;
	lite2d::Size const& size = this->getFrame().size;
	lite2d::Size mapSize = lite2d::makeSize(GRID_SIZE*tileMap.getWidth(),
		GRID_SIZE*tileMap.getHeight());

	this->miOffsetX = (size.width - mapSize.width) * 0.46f;
	this->miOffsetY = (size.height - mapSize.height) * 0.5f;

	this->mbFlagMode = false;
	this->setCurTileNull();

	this->mbCanMoveX = mapSize.width < size.width ? false : true;
	this->mbCanMoveY = mapSize.height < size.height ? false : true;
}

void GameView::setCurTileNull() {
	this->mCurrTile = lite2d::makePoint(-1, -1);
}

void GameView::onAttach() {
	this->registerKeyCtrl();
	this->registerTouch();

lite2d::registerHandler(TileMapProxy::GameStartEvent,
		new lite2d::Handler<GameView>(this, &GameView::handleEvent));
	lite2d::registerHandler(TileMapProxy::GameOverEvent,
		new lite2d::Handler<GameView>(this, &GameView::handleEvent));
	lite2d::registerHandler(TileMapProxy::GameClearEvent,
		new lite2d::Handler<GameView>(this, &GameView::handleEvent));
}

void GameView::onDetach() {
	lite2d::removeHandler(TileMapProxy::GameStartEvent, this);
	lite2d::removeHandler(TileMapProxy::GameOverEvent, this);
	lite2d::removeHandler(TileMapProxy::GameClearEvent, this);

	this->removeTouch();
	this->removeKeyCtrl();
}

void GameView::handleEvent(lite2d::Event const& args) {

	if (args.msgType() == TileMapProxy::GameStartEvent) {

		this->initGame();
		
	}
}

void GameView::onKeyDown(lite2d::APP_CTRL_TYPE key) {

	TileMapProxy& tileMap = *this->mpTileMap;

	if (tileMap.isGameOver() || tileMap.isGameClear()) {

		if (key == lite2d::APP_CTRL_START) {
			// nothing to do
			return;
		}
		
	}else {

		if (key == lite2d::APP_CTRL_LTRIGGER) {
			this->mbFlagMode = !this->mbFlagMode;
			return;
		}

		if (key == lite2d::APP_CTRL_TRIANGLE) {
			// nothing to do
			return;
		}
	}
}

void GameView::onKeyUp(lite2d::APP_CTRL_TYPE key) {
	
}

bool GameView::onTouchBegin(int x, int y) {

	this->setCurTileNull();
	this->mLastPt = lite2d::makePoint(x, y);

	TileMapProxy& tileMap = *this->mpTileMap;
	int tilex = 0, tiley = 0;
	getCoord(x-this->miOffsetX, y-this->miOffsetY, &tilex, &tiley);
	if (!tileMap.isValidCoord(tilex, tiley)) {
		return true;
	}

	if (tileMap.isGameOver()) {
		return true;
	}

	this->mCurrTile.x = tilex;
	this->mCurrTile.y = tiley;

	return true;
}

void GameView::onTouchMoved(int x, int y) {

	lite2d::Point lastPt = this->mLastPt;
	this->mLastPt = lite2d::makePoint(x, y);

	if (fabs(x-lastPt.x) > 16 || fabs(y-lastPt.y) > 16) {
		this->setCurTileNull();
	}

	if (this->mbCanMoveX) {
		this->miOffsetX += x - lastPt.x;
	}

	if (this->mbCanMoveY) {
		this->miOffsetY += y - lastPt.y;
	}
	
	TileMapProxy& tileMap = *this->mpTileMap;
	lite2d::Size const& size = this->getFrame().size;
	lite2d::Size mapSize = lite2d::makeSize(GRID_SIZE*tileMap.getWidth(),
		GRID_SIZE*tileMap.getHeight());

	if (mapSize.width < size.width) {
		if (this->miOffsetX < 0) {
			this->miOffsetX = 0;
		}else if (this->miOffsetX > size.width - mapSize.width) {
			this->miOffsetX = size.width - mapSize.width;
		}
	}else {
		if (this->miOffsetX < size.width - mapSize.width) {
			this->miOffsetX = size.width - mapSize.width;
		}else if (this->miOffsetX > 0) {
			this->miOffsetX = 0;
		}
	}

	if (mapSize.height < size.height) {
		if (this->miOffsetY < 0) {
			this->miOffsetY = 0;
		}else if (this->miOffsetY > size.height - mapSize.height) {
			this->miOffsetY = size.height - mapSize.height;
		}
	}else {
		if (this->miOffsetY < size.height - mapSize.height) {
			this->miOffsetY = size.height - mapSize.height;
		}else if (this->miOffsetY > 0) {
			this->miOffsetY = 0;
		}
	}
}

void GameView::onTouchEnded(int x, int y) {

	TileMapProxy& tileMap = *this->mpTileMap;
	int tilex = 0, tiley = 0;
	getCoord(x-this->miOffsetX, y-this->miOffsetY, &tilex, &tiley);
	if (!tileMap.isValidCoord(tilex, tiley)) {
		return;
	}

	lite2d::Point tilePt = this->mCurrTile;
	this->setCurTileNull();
	if (tilex != tilePt.x || tiley != tilePt.y) {
		return;
	}

	if (tileMap.isGameOver()) {
		return;
	}

	if (tileMap.isOpen(tilex, tiley) && tileMap.isNumberTile(tilex, tiley)) {
		// 点击数字自动翻开
		tileMap.autoOpen(tilex, tiley);
	}else {
		if (this->mbFlagMode) {
			// 标记模式
			tileMap.flagTile(tilex, tiley);
		}else {
			// 排雷模式
			tileMap.openTile(tilex, tiley);
		}
	}
}

void GameView::onRender() {

	this->showGrid();

	lite2d::Point const& pos = this->getWorldPos();
	lite2d::Size const& size = this->getFrame().size;
	lite2d::Point origin = this->getWorldPos();
	TileMapProxy& tileMap = *this->mpTileMap;

	origin.x += this->miOffsetX;
	origin.y += this->miOffsetY;

	for (int h = 0; h < tileMap.getHeight(); ++h) {

		// 排除不能显示的块
		if (origin.y+h*GRID_SIZE < pos.y-GRID_SIZE || origin.y > pos.y+size.height) {
			continue;
		}

		for (int w = 0; w < tileMap.getWidth(); ++w) {

			// 排除不能显示的块
			if (origin.x+w*GRID_SIZE < pos.x-GRID_SIZE || origin.x > pos.x+size.width) {
				continue;
			}

			TileData const& tile = tileMap[h][w];
			// 没有翻开时
			if (tile.tileState == TILE_STATE_CLOSE) {
				// 标记了旗子则显示旗子
				if (tile.tileTag == TILE_TAG_FLAG) {
					this->mpRender->draw(texTileD, origin.x+w*GRID_SIZE+1, origin.y+h*GRID_SIZE+1,
						GRID_SIZE-2, GRID_SIZE-2);
				}else {
					// 如果选中了当前块，显示按下效果
					if (w == this->mCurrTile.x && h == this->mCurrTile.y) {
						// 显示按下效果
						this->mpRender->draw(texTileDown, origin.x+w*GRID_SIZE+1, origin.y+h*GRID_SIZE+1,
							GRID_SIZE-2, GRID_SIZE-2);
						continue;
					}
					// 如果是标旗模式, 显示提示
					if (this->mbFlagMode) {
						this->mpRender->draw(texTileF, origin.x+w*GRID_SIZE+1, origin.y+h*GRID_SIZE+1,
							GRID_SIZE-2, GRID_SIZE-2);
					}else {
						// 默认显示遮罩
						this->mpRender->draw(texTileMask, origin.x+w*GRID_SIZE+1, origin.y+h*GRID_SIZE+1,
							GRID_SIZE-2, GRID_SIZE-2);
					}
				}
				continue;
			}else if (tile.tileState == TILE_STATE_SHOW_TIPS) {
				// 显示地雷提示
				this->mpRender->draw(texTileB2, origin.x+w*GRID_SIZE+1, origin.y+h*GRID_SIZE+1,
					GRID_SIZE-2, GRID_SIZE-2);
				continue;
			}
			// 翻开的状态
			if (tile.tileId == TILE_TYPE_NUMBER) {
				// 显示数字
				if (tile.tileValue < 1 && tile.tileValue > 8) {
					return;
				}
				int index = tile.tileValue - 1;
				lite2d::Texture* tex = texNumber[index];
				this->mpRender->draw(tex, origin.x+w*GRID_SIZE+1, origin.y+h*GRID_SIZE+1,
					GRID_SIZE-2, GRID_SIZE-2);
			}else if (tile.tileId == TILE_TYPE_MINE) {
				// 显示地雷
				this->mpRender->draw(texTileB, origin.x+w*GRID_SIZE+1, origin.y+h*GRID_SIZE+1,
					GRID_SIZE-2, GRID_SIZE-2);
			}else {
				// 默认块
				this->mpRender->draw(texTileBase, origin.x+w*GRID_SIZE+1, origin.y+h*GRID_SIZE+1,
					GRID_SIZE-2, GRID_SIZE-2);
			}
		}
	}

	/*vita2d_pgf_draw_textf(gamefont, 960*0.4f, 20, RGBA8(255,255,255,255), 1.0f,
		"[MINE: %03d  CLOSE: %03d  TIME: %03d]",
		tileMap.getCurMineCount(), tileMap.getCurCloseCount(), this->miTimeSecond);*/
}

void GameView::showGrid() {
	TileMapProxy& tileMap = *this->mpTileMap;
	lite2d::Size size = lite2d::makeSize(GRID_SIZE*tileMap.getWidth(),
		GRID_SIZE*tileMap.getHeight());
	int woffset = 0, hoffset = 0;
	while(woffset <= size.width) {
		this->drawvline(lite2d::makePoint(woffset+this->miOffsetX, 0+this->miOffsetY), size.height, RGBA8(148,148,148,255));
	    woffset += GRID_SIZE;
	}
	while(hoffset <= size.height) {
		this->drawhline(lite2d::makePoint(0+this->miOffsetX, hoffset+this->miOffsetY), size.width, RGBA8(148,148,148,255));
	    hoffset += GRID_SIZE;
	}
}
