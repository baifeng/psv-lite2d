
#ifndef LITE2D_FONT_H
#define LITE2D_FONT_H

#include "stdio.h"
#include "string"
#include "lite2dObject.h"
#include "lite2dStruct.h"

namespace lite2d {

#define DEFAULT_FONT_SIZE (15.0)

class Font : public Ref {
public:
	Font();
	virtual ~Font();
public:
	virtual Size getTextSize(std::string const& text);
	virtual Size getTextSize(std::string const& text, float scale);
public:
	float getFontScale() const {
		return this->fontScale;
	}
	void setFontScale(float scale) {
		this->fontScale = scale;
	}
	unsigned int getFontColor() const {
		return this->fontColor;
	}
	void setFontColor(unsigned int color) {
		this->fontColor = color;
	}
public:
	virtual void* getFont() const;
protected:
	virtual void freeFont();
protected:
	void* font;
	unsigned int fontColor;
	float fontScale;
};

} // lite2d

#endif // LITE2D_FONT_H
