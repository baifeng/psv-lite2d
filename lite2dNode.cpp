
#include "lite2dNode.h"
#include "lite2dMacro.h"
#include "lite2dRender.h"

#include "lite2dAppContent.h"
#include "lite2dDispatch.h"
#include "lite2dResponder.h"
#include "lite2dTexture.h"

namespace lite2d {

Node::Node() {
	this->mpRender = sharedRender();

	this->mbVisible = true;
	this->mbUpdate = false;
	this->mbBound = false;
	this->mbUseTouch = true;
	this->mbUseJoyBtn = true;
	this->mbUseJoyAxis = true;

	this->miTag = -1;
	this->miColor = 0;
	this->mfAngle = 0.0f;

	this->mpParent = NULL;
	this->mPosition = makePoint(960*0.5f, 544*0.5f);
	this->mWorldPos = makePoint(0, 0);
	this->mAnchor = makePoint(0.5f, 0.5f);
	this->mScale  = makePoint(1.0f, 1.0f);
	this->mFrame  = makeRect(0, 0, 960, 544);
}

Node::~Node() {
	this->mpParent = NULL;
	for (std::list<Node*>::iterator i = this->mChildlist.begin(); i != this->mChildlist.end(); ++i) {
		if ((*i)!=NULL) {
			(*i)->detach();
			delete *i;
		}
	}
	this->mChildlist.clear();
}

void Node::onAttach() {}
void Node::onDetach() {}

void Node::onUpdate(float dt) {}
void Node::onRender() {}
void Node::onKeyDown(APP_CTRL_TYPE key) {
	/*const char* btn_label[]={"SELECT ","","","START ",
		"UP ","RIGHT ","DOWN ","LEFT ","L ","R ","","",
		"TRIANGLE ","CIRCLE ","CROSS ","SQUARE "};
	for(int i=0; i < sizeof(btn_label)/sizeof(*btn_label); i++) {
		if (key & (1<<i)) {
			add_debug_text(RGBA8(255,255,255,255), "%s down.", btn_label[i]);
			break;
		}
	}*/
}
void Node::onKeyUp(APP_CTRL_TYPE key) {
	/*const char* btn_label[]={"SELECT ","","","START ",
		"UP ","RIGHT ","DOWN ","LEFT ","L ","R ","","",
		"TRIANGLE ","CIRCLE ","CROSS ","SQUARE "};
	for(int i=0; i < sizeof(btn_label)/sizeof(*btn_label); i++) {
		if (key & (1<<i)) {
			add_debug_text(RGBA8(255,255,255,255), "%s up.", btn_label[i]);
			break;
		}
	}*/
}
void Node::onAxisMoveL(int x, int y) {}
void Node::onAxisMoveR(int x, int y) {}
bool Node::onTouchBegin(int x, int y) {
	/*add_debug_text(RGBA8(255,255,255,255), "touch begin x:%.2f, y:%.2f",(float)x/this->mFrame.size.width, (float)y/this->mFrame.size.height);
	return true;*/
	return true;
}
void Node::onTouchMoved(int x, int y) {
	/*add_debug_text(RGBA8(255,255,255,255), "touch moved x:%.2f, y:%.2f",(float)x/this->mFrame.size.width, (float)y/this->mFrame.size.height);*/
}
void Node::onTouchEnded(int x, int y) {
	/*add_debug_text(RGBA8(255,255,255,255), "touch end x:%.2f, y:%.2f",(float)x/this->mFrame.size.width, (float)y/this->mFrame.size.height);*/
}

void Node::showBounds(bool b) {
	mbBound = b;
}

void Node::setNodeColor(unsigned int color) {
	this->miColor = color;
}

void Node::setPosition(float x, float y) {
	this->mPosition.x = x;
	this->mPosition.y = y;
	this->mFrame.origin.x = x - this->mFrame.size.width * this->mAnchor.x;
	this->mFrame.origin.y = y - this->mFrame.size.height * this->mAnchor.y;
}

Point const& Node::getPosition() const {
	return this->mPosition;
}

void Node::setFrame(float x, float y, float width, float height) {
	this->setFrame(makeRect(x, y, width, height));
}

void Node::setFrame(Rect const& frame) {
	this->mFrame = frame;
	this->mPosition = makePoint(frame.origin.x+frame.size.width*this->mAnchor.x,
		frame.origin.y+frame.size.height*this->mAnchor.y);
}

Rect const& Node::getFrame() const {
	return this->mFrame;
}

Point const& Node::getWorldPos() const {
	return this->mWorldPos;
}

void Node::setAnchor(Point const& anchor) {
	this->mAnchor = anchor;
	this->setFrame(this->getFrame());
}

Point const& Node::getAnchor() const {
	return this->mAnchor;
}

void Node::setScale(Point const& scale) {
	this->mScale = scale;
}

Point const& Node::getScale() const {
	return this->mScale;
}

void Node::setAngle(float f) {
	this->mfAngle = f;
}

float Node::getAngle() const {
	return this->mfAngle;
}

void Node::setVisible(bool b) {
	this->mbVisible = b;
}

bool Node::isVisible() const {
	return this->mbVisible;
}

void Node::setTag(int tag) {
	this->miTag = tag;
}

int Node::getTag() const {
	return this->miTag;
}

void Node::update(float dt) {
	this->onUpdate(dt);
}

void Node::render() {
	if (!this->mbVisible) {
		return;
	}
	if (this->mpParent) {
		this->mWorldPos.x = this->mpParent->mWorldPos.x + this->mFrame.origin.x;
		this->mWorldPos.y = this->mpParent->mWorldPos.y + this->mFrame.origin.y;
	}else {
		this->mWorldPos = this->mFrame.origin;
	}
	if (this->miColor > 0) {
		this->mpRender->drawrect(this->mWorldPos.x, this->mWorldPos.y,
			this->mFrame.size.width, this->mFrame.size.height, this->miColor);
	}
	if (this->mbBound) {
		// top
		this->drawhline(makePoint(1, 1), this->mFrame.size.width-1);
		// left
		this->drawvline(makePoint(1, 1), this->mFrame.size.height-1);
		// right
		this->drawvline(makePoint(this->mFrame.size.width, 1), this->mFrame.size.height-1);
		// bottom
		this->drawhline(makePoint(1, this->mFrame.size.height), this->mFrame.size.width-1);
	}
	this->onRender();
	for (ChildList::iterator i = this->mChildlist.begin(); i != this->mChildlist.end(); ++i) {
		(*i)->render();
	}
}

void Node::drawvline(Point const& offset, int length) {
	this->drawvline(offset, length, RGBA8(255, 255, 255, 255));
}

void Node::drawhline(Point const& offset, int length) {
	this->drawhline(offset, length, RGBA8(255, 255, 255, 255));
}

void Node::drawvline(Point const& offset, int length, unsigned int color) {
	lite2d::Point const& origin = this->getWorldPos();
	this->mpRender->drawline(origin.x+offset.x, origin.y+offset.y,
			origin.x+offset.x, origin.y+offset.y+length, color);
}

void Node::drawhline(Point const& offset, int length, unsigned int color) {
	lite2d::Point const& origin = this->getWorldPos();
	this->mpRender->drawline(origin.x+offset.x, origin.y+offset.y,
			origin.x+offset.x+length, origin.y+offset.y, color);
}

void Node::attach() {
	this->onAttach();
}

void Node::detach() {
    this->removeKeyCtrl();
	this->removeTouch();
    this->removeUpdate();
	this->onDetach();
}

bool Node::addChild(Node* node) {
	node->removeFromParent();
	if (node->getParent()) {
		return false;
	}
	this->mChildlist.push_back(node);
	node->mpParent = this;
	node->attach();
	return true;
}

bool Node::removeChild(Node* node) {
	for (ChildList::iterator i = this->mChildlist.begin(); i != this->mChildlist.end(); ++i) {
		if ((*i)==node) {
			node->detach();
			node->mpParent = NULL;
			this->mChildlist.erase(i);
			return true;
		}
	}
	return false;
}

bool Node::removeFromParent() {
	if (this->getParent()==NULL) {
		return false;
	}
	return this->getParent()->removeChild(this);
}

Node* Node::getChildWithTag(int tag) {
    if (tag==-1) {
        return NULL;
    }
	for (ChildList::iterator i = this->mChildlist.begin(); i != this->mChildlist.end(); ++i) {
		if ((*i)->getTag()==tag) {
			return *i;
		}
	}
	return NULL;
}

Node* Node::getParent() const {
	return this->mpParent;
}

void Node::setTouchEnable(bool b) {
	this->mbUseTouch = b;
}

bool Node::isTouchEnable() const {
	return this->mbUseTouch;
}

void Node::setJoyBtnEnable(bool b) {
	this->mbUseJoyBtn = b;
}

bool Node::isJoyBtnEnable() const {
	return this->mbUseJoyBtn;
}

void Node::setJoyAxisEnable(bool b) {
	this->mbUseJoyAxis = b;
}

bool Node::isJoyAxisEnable() const {
	return this->mbUseJoyAxis;
}

bool Node::canBeTouch() const {
	if (!this->isVisible() || !this->isTouchEnable()) {
		return false;
	}

	Node* parent = this->getParent();
	while(parent!=NULL) {
	    if (!parent->isVisible() || !parent->isTouchEnable()) {
			return false;
		}
		parent = parent->getParent();
	}

	return true;
}

bool Node::canBeHit(int local_x, int local_y) const {
	if (!this->is_hit(local_x, local_y)) {
		return false;
	}

	int world_x = 0, world_y = 0;
	this->convertLocalPoint(local_x, local_y, &world_x, &world_y);

	Node* parent = this->getParent();
	while(parent!=NULL) {
		parent->convertWorldPoint(world_x, world_y, &local_x, &local_y);
	    if (!parent->is_hit(local_x, local_y)) {
			return false;
		}
		parent = parent->getParent();
	}

	return true;
}

bool Node::is_hit(int local_x, int local_y) const {
	if (local_x >= 0 && local_x <= this->mFrame.size.width && local_y >= 0 && local_y <= this->mFrame.size.height) {
		return true;
	}
	return false;
}

void Node::convertWorldPoint(int world_x, int world_y, int* local_x, int* local_y) const {
	*local_x = world_x - this->mWorldPos.x;
	*local_y = world_y - this->mWorldPos.y;
}

void Node::convertLocalPoint(int local_x, int local_y, int* world_x, int* world_y)  const {
	*world_x = this->mWorldPos.x + local_x;
	*world_y = this->mWorldPos.y + local_y;
}

void Node::registerTouch() {
	AppContent& content = AppContent::instance();
	TouchHolder& touch = content.getTouchHolder();
	touch.registerResponder(this);
}

void Node::removeTouch() {
	AppContent& content = AppContent::instance();
	TouchHolder& touch = content.getTouchHolder();
	touch.removeResponder(this);
}

void Node::registerKeyCtrl() {
    AppContent& content = AppContent::instance();
    KeyCtrlHolder& keyCtrl = content.getKeyCtrlHolder();
    keyCtrl.registerResponder(this);
}

void Node::removeKeyCtrl() {
    AppContent& content = AppContent::instance();
    KeyCtrlHolder& keyCtrl = content.getKeyCtrlHolder();
    keyCtrl.removeResponder(this);
}

void Node::registerUpdate() {
	AppContent& content = AppContent::instance();
	DispatcherHolder& dispatch = content.getDispatcherHolder();
	dispatch.registerDispatcher(new NodeDispatcher(this));
}

void Node::removeUpdate() {
	AppContent& content = AppContent::instance();
	DispatcherHolder& dispatch = content.getDispatcherHolder();
	dispatch.removeDispatcher(NodeDispatcher::makeKey(this));
}


NodePopEffect::NodePopEffect(Node* child) {
	this->mkAnimateStyle = ANIMATE_NONE;
	this->mbPlaying = false;
	this->mCallBack = NULL;
	this->mpTarget = child;
	if (child != NULL) {
		this->addChild(child);
		child->setVisible(false);
	}
	this->mpBG = NULL;
}

NodePopEffect::~NodePopEffect() {
	this->mpTarget = NULL;
	this->mCallBack = NULL;
	this->setTexture(NULL);
}

void NodePopEffect::setTexture(Texture* tex) {
	SAFE_RELEASE(this->mpBG);
	this->mpBG = tex;
	SAFE_RETAIN(this->mpBG);
}

void NodePopEffect::setCompletedCallBack(FinishFunc func) {
	this->mCallBack = func;
}

void NodePopEffect::stopAnimation() {
	if (this->mbPlaying) {
		this->mbPlaying = false;
		this->setFrame(this->mOriginFrame);
	}
}

bool NodePopEffect::playAnimation(AnimateStyle style, float duration) {
	if (this->mbPlaying) {
		return false;
	}
	this->mbPlaying = true;
	this->mfScale = 0.0f;
	this->mfCurValue = 0.0f;
	this->mfDuration = duration;
	this->mkAnimateStyle = style;
	this->mOriginFrame = this->getFrame();
	this->registerUpdate();
	this->setVisible(true);
	if (this->mpTarget) {
		this->mpTarget->setVisible(false);
	}
	return true;
}

void NodePopEffect::onAttach() {
	//this->registerKeyCtrl();
}

void NodePopEffect::onDetach() {
	//this->removeKeyCtrl();
}

void NodePopEffect::onKeyDown(APP_CTRL_TYPE key) {
	static AnimateStyle style = ANIMATE_IN;
	if (this->isPlaying()) {
		return;
	}
	if (key == APP_CTRL_TRIANGLE) {
		this->setAnchor({0, 0});
	}
	if (key == APP_CTRL_CROSS) {
		this->setAnchor({1, 0});
	}
	if (key == APP_CTRL_SQUARE) {
		this->setAnchor({0, 1});
	}
	if (key == APP_CTRL_CIRCLE) {
		this->setAnchor({1, 1});
	}
	if (key == APP_CTRL_RTRIGGER) {
		style = ANIMATE_IN;
	}
	if (key == APP_CTRL_LTRIGGER) {
		style = ANIMATE_OUT;
	}
	this->playAnimation(style, 0.25f);
}

void NodePopEffect::onUpdate(float dt) {

	this->mfCurValue += dt;
	this->mfScale = this->mfCurValue / this->mfDuration;

	if (this->mfScale >= 1.0f) {
		this->mfScale = 1.0f;
		this->mbPlaying = false;
		this->removeUpdate();
		this->setFrame(this->mOriginFrame);
		if (this->mkAnimateStyle == ANIMATE_OUT) {
			this->setVisible(false);
		}
		if (this->mpTarget && this->mkAnimateStyle==ANIMATE_IN) {
			this->mpTarget->setVisible(true);
		}
		if (this->mCallBack) {
			this->mCallBack(this);
		}
		return;
	}

	if (this->mkAnimateStyle == ANIMATE_NONE) {
		return;
	}

	Point pt = this->getPosition();
	Rect r = this->mOriginFrame;
	float scale = this->mkAnimateStyle == ANIMATE_OUT ? 1.0f-this->mfScale : this->mfScale;
	r.size.width *= scale;
	r.size.height *= scale;
	this->setFrame(r);
	this->setPosition(pt.x, pt.y);
}

void NodePopEffect::onRender() {
	if (this->mpBG==NULL) {
		return;
	}
	Point const& pos = this->getWorldPos();
	Size const& size = this->getFrame().size;
	this->mpRender->draw(this->mpBG, pos.x, pos.y, size.width, size.height);
}

}
