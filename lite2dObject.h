
#ifndef LITE2D_OBJECT_H
#define LITE2D_OBJECT_H

#include "stdio.h"

namespace lite2d {

class Ref
{
public:
	Ref();
	virtual ~Ref();
public:
	void retain();
	void release();
public:
	int refCount() const {
		return this->miRetainCount;
	}
private:
	int miRetainCount;
};

class Object : public Ref {
public:
	virtual ~Object();
};

} // lite2d

#endif // LITE2D_OBJECT_H
