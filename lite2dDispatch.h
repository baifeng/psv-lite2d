
#ifndef LITE2D_DISPATCH_H
#define LITE2D_DISPATCH_H

#include "stdio.h"
#include "string"
#include "map"
#include "functional"

namespace lite2d {

class DispatcherHolder;
class Dispatcher {
public:
	Dispatcher() {
		this->mpHolder = NULL;
	}
	virtual ~Dispatcher(){}
public:
	void setHolder(DispatcherHolder* holder) {
		this->mpHolder = holder;
	}
	void setDispatchKey(std::string const& key) {
		this->mDispatchKey = key;
	}
public:
	std::string const& getDispatchKey() const {
		return this->mDispatchKey;
	}
	void finish(); // finish call
public:
	virtual void update(float dt){}
private:
	DispatcherHolder* mpHolder;
	std::string mDispatchKey;
};

class DispatcherHolder {
public:
	typedef std::map<std::string, Dispatcher*> DispatcherMap;
public:
	DispatcherHolder();
	~DispatcherHolder();
public:
	bool registerDispatcher(Dispatcher* target);
	bool removeDispatcher(std::string const& name);
public:
	void update(float dt);
private:
	DispatcherMap mDispatcherMap;
};

} // lite2d

#endif // LITE2D_DISPATCH_H
