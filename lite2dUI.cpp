
#include "lite2dUI.h"
#include "lite2dFont.h"
#include "lite2dTexture.h"
#include "lite2dAppContent.h"
#include "lite2dRender.h"
#include "lite2dMacro.h"
#include "string.h"

namespace lite2d {

Sprite::Sprite() {
	this->mpTex = NULL;
}

Sprite::~Sprite() {
	this->setTexture(NULL);
}

bool Sprite::loadTexture(std::string const& fileName) {
	AppContent& content = AppContent::instance();
	Texture* tex = content.getTextureCache().getTexture(fileName);
	this->setTexture(tex);
	return tex != NULL;
}

void Sprite::setTexture(Texture* tex) {
	SAFE_RELEASE(this->mpTex);
	this->mpTex = tex;
	SAFE_RETAIN(this->mpTex);
}

void Sprite::onRender() {
	if (this->mpTex==NULL) {
		return;
	}
	Point const& pos = this->getWorldPos();
	Size const& size = this->getFrame().size;
	this->mpRender->draw(this->mpTex, pos.x, pos.y, size.width, size.height);
}


Label::Label() {
	this->mpFont = new Font();
}

Label::~Label() {
	SAFE_RELEASE(this->mpFont);
}

void Label::setFontScale(float scale) {
	if (this->mpFont) {
		this->mpFont->setFontScale(scale);
	}
}

float Label::getFontScale() const {
	if (this->mpFont) {
		return this->mpFont->getFontScale();
	}
	return 1.0f;
}

void Label::setTextColor(unsigned int color) {
	if (this->mpFont) {
		this->mpFont->setFontColor(color);
	}
}

unsigned int Label::getTextColor() const {
	if (this->mpFont) {
		return this->mpFont->getFontColor();
	}
	return 0;
}

void Label::setText(std::string const& text) {
	this->mText = text;
	if (this->mpFont) {
		Size size = this->mpFont->getTextSize(this->mText);
		Point pos = this->getPosition();
		this->setFrame(0, 0, size.width, size.height);
		this->setPosition(pos.x, pos.y);
	}
}

void Label::onRender() {
	if (this->mpFont==NULL || this->mText.length()==0) {
		return;
	}
	Point const& pt = this->getWorldPos();
	this->mpRender->drawtext(this->mpFont, pt.x, pt.y, this->mText.c_str());
}


Button::Button() {
	this->mkCurState = STATE_UNKNOWN;
	this->mpTitle = NULL;
	memset(this->mpTex, 0, sizeof(this->mpTex));
	memset(this->mColor, 0, sizeof(this->mColor));
	this->mColor[0] = RGBA8(0, 0, 0, 255);
	this->mText[0] = "Button";
	
	this->mDownFunc = NULL;
	this->mUpFunc = NULL;

	this->mpTitle = new Label();
	this->mpTitle->setTextColor(this->mColor[0]);
	this->mpTitle->setText(this->mText[0]);
	this->addChild(this->mpTitle);
}

Button::~Button() {
	SAFE_RELEASE(this->mpTex[0]);
	SAFE_RELEASE(this->mpTex[1]);
	SAFE_RELEASE(this->mpTex[2]);
	this->mDownFunc = NULL;
	this->mUpFunc = NULL;
}

void Button::setTitleColor(unsigned int color, State kState) {
	this->mColor[kState] = color;
}

void Button::setTitle(std::string const& title, State kState) {
	this->mText[kState] = title;
}

void Button::setImage(Texture* tex, State kState) {
	SAFE_RELEASE(this->mpTex[kState]);
	this->mpTex[kState] = tex;
	SAFE_RETAIN(this->mpTex[kState]);
}

unsigned int Button::titleColorWithState(State kState) const {
	return this->mColor[kState];
}

std::string const& Button::titleWithState(State kState) const {
	return this->mText[kState];
}

Texture* Button::imageWithState(State kState) const {
	return this->mpTex[kState];
}

void Button::setEnable(bool b) {
	this->setTouchEnable(b);
	this->setState(b ? STATE_NORMAL : STATE_DISABLED);
}

bool Button::isEnable() const {
	return this->isTouchEnable();
}

void Button::setDownFunc(TouchFunc func) {
	this->mDownFunc = func;
}

void Button::setUpFunc(TouchFunc func) {
	this->mUpFunc = func;
}

Label* Button::getTitleView() const {
	return this->mpTitle;
}

void Button::setState(State kState) {
	this->mkCurState = kState;

	if (this->mColor[kState]==0) {
		this->mpTitle->setTextColor(this->mColor[STATE_NORMAL]);
	}else {
		this->mpTitle->setTextColor(this->mColor[kState]);
	}
	if (this->mText[kState].length()==0) {
		this->mpTitle->setText(this->mText[STATE_NORMAL]);
	}else {
		this->mpTitle->setText(this->mText[kState]);
	}
	this->mpTitle->setPosition(this->getFrame().size.width*0.5f,
		this->getFrame().size.height*0.5f);
}

void Button::onAttach() {
	this->registerTouch();

	if (this->mkCurState == STATE_UNKNOWN) {
		this->setState(STATE_NORMAL);
	}
}

void Button::onDetach() {
	this->removeTouch();
}

void Button::onRender() {
	Texture* curr_tex = NULL;
	if (this->mpTex[this->mkCurState] != NULL) {
		curr_tex = this->mpTex[this->mkCurState];
	}else if (this->mpTex[STATE_NORMAL] != NULL) {
		curr_tex = this->mpTex[STATE_NORMAL];
	}
	if (curr_tex==NULL) {
		return;
	}
	SAFE_RETAIN(curr_tex);
	Point const& pos = this->getWorldPos();
	Size const& size = this->getFrame().size;
	this->mpRender->draw(curr_tex, pos.x, pos.y, size.width, size.height);
	SAFE_RELEASE(curr_tex);
}

bool Button::onTouchBegin(int x, int y) {
	this->setState(STATE_HIGHLIGHT);
	if (this->mDownFunc) {
		this->mDownFunc(this);
	}
	return true;
}

void Button::onTouchMoved(int x, int y) {
	if (x < -80 || x > this->getFrame().size.width+80 || y < -80 || y > this->getFrame().size.height+80) {
		if (this->mkCurState == STATE_HIGHLIGHT) {
			this->setState(STATE_NORMAL);
		}
	}else {
		if (this->mkCurState == STATE_NORMAL) {
			this->setState(STATE_HIGHLIGHT);
		}
	}
}

void Button::onTouchEnded(int x, int y) {
	if (this->mkCurState == STATE_HIGHLIGHT) {
		this->setState(STATE_NORMAL);
	}
	if (x >= 0 && x <= this->getFrame().size.width && y >= 0 && y <= this->getFrame().size.height) {
		if (this->mUpFunc) {
			this->mUpFunc(this);
		}
	}
}

}
