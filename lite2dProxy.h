
#ifndef LITE2D_PROXY_H
#define LITE2D_PROXY_H

#include "lite2dEvent.h"
#include "lite2dAppContent.h"

namespace lite2d {

class Proxy {
public:
	Proxy(std::string const& name) {
		this->mProxyName = name;
		this->mpHandlerHolder = &AppContent::instance().getHandlerHolder();
	}
	virtual ~Proxy() {
		this->mpHandlerHolder = NULL;
	}
public:
	std::string const& getProxyName() const {
		return this->mProxyName;
	}
	void notify(Event const& args) {
		this->mpHandlerHolder->notify(args);
	}
public:
	virtual void onAttach(){}
	virtual void onDetach(){}
private:
	HandlerHolder* mpHandlerHolder;
	std::string mProxyName;
};

class ProxyHolder {
public:
	typedef std::map<std::string, Proxy*> ProxyMap;
public:
	ProxyHolder();
	~ProxyHolder();
public:
	bool registerProxy(Proxy* proxy);
	bool removeProxy(std::string const& proxyName);
	Proxy& getProxy(std::string const& proxyName);
private:
	ProxyMap mProxyMap;
};

} // lite2d

#endif // LITE2D_PROXY_H
