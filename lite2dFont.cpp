
#include "lite2dFont.h"
#include "lite2dMacro.h"
#include "vita2d.h"

namespace lite2d {

static vita2d_pgf* def_pfg = NULL;

Font::Font() {
	if (def_pfg == NULL) {
		def_pfg = vita2d_load_default_pgf();
	}
	this->font = def_pfg;
	this->fontColor = RGBA8(0, 0, 0, 255);
	this->fontScale = 1.0f;
}

Font::~Font() {
	this->freeFont();
}

void Font::freeFont() {
	/*if (this->font != NULL) {
		vita2d_free_pgf((vita2d_pgf*)this->font);
		this->font = NULL;
	}*/
}

void* Font::getFont() const {
	return this->font;
}

Size Font::getTextSize(std::string const& text) {
	return this->getTextSize(text, this->fontScale);
}

Size Font::getTextSize(std::string const& text, float scale) {
	if (this->font == NULL) {
		return makeSize(0, 0);
	}
	int w = vita2d_pgf_text_width((vita2d_pgf*)this->font, scale, text.c_str());
	int h = vita2d_pgf_text_height((vita2d_pgf*)this->font, scale, text.c_str());
	return makeSize(w, h);
}

} // lite2
