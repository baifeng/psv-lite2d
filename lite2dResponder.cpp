
#include "lite2dResponder.h"

namespace lite2d {

TouchHolder::TouchHolder() {
	this->mpCurResponder = NULL;
}

TouchHolder::~TouchHolder() {
	this->mpCurResponder = NULL;
	this->mResponderlist.clear();
}

bool TouchHolder::registerResponder(TouchResponder* responder) {
	for (ResponderList::iterator i = this->mResponderlist.begin(); i != this->mResponderlist.end(); ++i) {
		if ((*i)==responder) {
			return false;
		}
	}
	this->mResponderlist.push_back(responder);
	return true;
}

bool TouchHolder::removeResponder(TouchResponder* responder) {
	for (ResponderList::iterator i = this->mResponderlist.begin(); i != this->mResponderlist.end(); ++i) {
		if ((*i)==responder) {
			this->mResponderlist.erase(i);
			return true;
		}
	}
	return false;
}

bool TouchHolder::touchBegin(int world_x, int world_y) {
	if (this->mpCurResponder!=NULL) {
		int x = 0, y = 0;
		this->mpCurResponder->convertWorldPoint(world_x, world_y, &x, &y);
		this->mpCurResponder->onTouchEnded(x, y);
		this->mpCurResponder = NULL;
	}

	bool ret = false;
	int x = 0, y = 0;
	ResponderList temp = this->mResponderlist;
	for (ResponderList::reverse_iterator i = temp.rbegin(); i != temp.rend(); ++i) {
		TouchResponder* responder = (*i);
		responder->convertWorldPoint(world_x, world_y, &x, &y);
		if (responder->canBeTouch() && responder->canBeHit(x, y)) {
			ret = responder->onTouchBegin(x, y);
			if (ret) {
				this->mpCurResponder = responder;
				break;
			}
		}
	}

	return this->mpCurResponder != NULL;
}

void TouchHolder::touchMoved(int world_x, int world_y) {
	if (this->mpCurResponder==NULL) {
		return;
	}
	int x = 0, y = 0;
	this->mpCurResponder->convertWorldPoint(world_x, world_y, &x, &y);
	this->mpCurResponder->onTouchMoved(x, y);
}

void TouchHolder::touchEnded(int world_x, int world_y) {
	if (this->mpCurResponder==NULL) {
		return;
	}
	int x = 0, y = 0;
	this->mpCurResponder->convertWorldPoint(world_x, world_y, &x, &y);
	this->mpCurResponder->onTouchEnded(x, y);
	this->mpCurResponder = NULL;
}


KeyCtrlHolder::KeyCtrlHolder() {
	this->mpCurResponder = NULL;
}

KeyCtrlHolder::~KeyCtrlHolder() {
	this->mpCurResponder = NULL;
	this->mResponderlist.clear();
}

bool KeyCtrlHolder::registerResponder(KeyResponder* responder) {
	for (ResponderList::iterator i = this->mResponderlist.begin(); i != this->mResponderlist.end(); ++i) {
		if ((*i)==responder) {
			return false;
		}
	}
	this->mpCurResponder = responder;
	this->mResponderlist.push_back(responder);
	return true;
}

bool KeyCtrlHolder::removeResponder(KeyResponder* responder) {
	for (ResponderList::iterator i = this->mResponderlist.begin(); i != this->mResponderlist.end(); ++i) {
		if ((*i)==responder) {
			this->mResponderlist.erase(i);
			this->mpCurResponder = this->mResponderlist.back();
			return true;
		}
	}
	return false;
}

static APP_CTRL_TYPE keyState[] = {
	APP_CTRL_SELECT,
	APP_CTRL_START,
	APP_CTRL_UP,
	APP_CTRL_RIGHT,
	APP_CTRL_DOWN,
	APP_CTRL_LEFT,
	APP_CTRL_LTRIGGER,
	APP_CTRL_RTRIGGER,
	APP_CTRL_TRIANGLE,
	APP_CTRL_CIRCLE,
	APP_CTRL_CROSS,
	APP_CTRL_SQUARE,
	APP_CTRL_ANY
};

void KeyCtrlHolder::keyDown(int key) {
	if (this->mpCurResponder && this->mpCurResponder->isJoyBtnEnable()) {
        for (int i = 0; i < APP_CTRL_SIZE_MAX; ++i) {
            if (!this->mpCurResponder->isKeyPressed(keyState[i]) && (key & keyState[i])) {
                this->mpCurResponder->setKeyState(keyState[i], true);
                this->mpCurResponder->onKeyDown(keyState[i]);
            }
        }
	}
}

void KeyCtrlHolder::keyUp(int key) {
	if (this->mpCurResponder && this->mpCurResponder->isJoyBtnEnable()) {
        for (int i = 0; i < APP_CTRL_SIZE_MAX; ++i) {
            if (this->mpCurResponder->isKeyPressed(keyState[i]) && (key & keyState[i])) {
                this->mpCurResponder->setKeyState(keyState[i], false);
                this->mpCurResponder->onKeyUp(keyState[i]);
            }
        }
	}
}

void KeyCtrlHolder::axisMoveL(int x, int y) {
	if (this->mpCurResponder && this->mpCurResponder->isJoyAxisEnable()) {
		this->mpCurResponder->onAxisMoveL(x, y);
	}
}

void KeyCtrlHolder::axisMoveR(int x, int y) {
	if (this->mpCurResponder && this->mpCurResponder->isJoyAxisEnable()) {
		this->mpCurResponder->onAxisMoveR(x, y);
	}
}

APP_CTRL_TYPE KeyCtrlHolder::getKeyState(int index) {
	return keyState[index];
}

} // lite2d