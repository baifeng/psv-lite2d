
#include "lite2dApp.h"
#include "lite2dMacro.h"
#include "lite2dAppContent.h"
#include "lite2dDispatch.h"
#include "lite2dResponder.h"
#include "lite2dProxy.h"
#include "lite2dNode.h"
#include "lite2dWorker.h"
#include "lite2dEvent.h"
#include "lite2dRender.h"
#include "lite2dTexture.h"

#include <psp2/ctrl.h>
#include <psp2/kernel/processmgr.h>
#include <psp2/touch.h>
#include <vita2d.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <vector>

#define lerp(value, from_max, to_max) ((((value * 10) * (to_max * 10)) / (from_max * 10)) / 10)

namespace lite2d {

bool exitVal = false;
bool showFps = false;

vita2d_pgf *pgf = NULL;

typedef struct InputContent {
	bool touch_pressed;
	SceCtrlData oldPad, newPad;
	SceTouchData oldTouch, newTouch;

	float axislx() const {
		return newPad.lx-127.5f;
	}
	float axisly() const {
		return newPad.ly-127.5f;
	}
	float axisrx() const {
		return newPad.rx-127.5f;
	}
	float axisry() const {
		return newPad.ry-127.5f;
	}
	int touchx() const {
		if (istouch()) {
			return lerp(newTouch.report[0].x, 1919, 960);
		}
		return 0;
	}
	int touchy() const {
		if (istouch()) {
			return lerp(newTouch.report[0].y, 1087, 544);
		}
		return 0;
	}
	bool istouch() const {
		return newTouch.reportNum > 0;
	}
	int otouchx() const {
		if (isotouch()) {
			return lerp(oldTouch.report[0].x, 1919, 960);
		}
		return 0;
	}
	int otouchy() const {
		if (isotouch()) {
			return lerp(oldTouch.report[0].y, 1087, 544);
		}
		return 0;
	}
	bool isotouch() const {
		return oldTouch.reportNum > 0;
	}
	bool istouch_moved() const {
		if (isotouch() && istouch()) {
			return touchx()!=otouchx() || touchy()!=otouchy();
		}
		return false;
	}
	bool isButtonPressed(unsigned int button) {
		if (!(oldPad.buttons & button)) {
			if (newPad.buttons & button) {
				return true;
			}
		}
		return false;
	}
	bool isButtonUp(unsigned int button) {
		if (oldPad.buttons & button) {
			if (!(newPad.buttons & button)) {
				return true;
			}
		}
		return false;
	}
	void InputProc() {
		oldPad = newPad;
		oldTouch = newTouch;
		sceCtrlPeekBufferPositive(0, &newPad, 1);
		sceTouchPeek(SCE_TOUCH_PORT_FRONT, &newTouch, 1);

		lite2d::AppContent& content = lite2d::AppContent::instance();
		TouchHolder& touch = content.getTouchHolder();
		KeyCtrlHolder& keyCtrl = content.getKeyCtrlHolder();

		int key = 0;

		for (int i = 0; i < APP_CTRL_SIZE_MAX; ++i) {
			if (isButtonPressed(keyCtrl.getKeyState(i))) {
				key = key | keyCtrl.getKeyState(i);
			}
		}
		if (key > 0) {
			keyCtrl.keyDown(key);
		}

		key = 0;

		for (int i = 0; i < APP_CTRL_SIZE_MAX; ++i) {
			if (isButtonUp(keyCtrl.getKeyState(i))) {
				key = key | keyCtrl.getKeyState(i);
			}
		}
		if (key > 0) {
			keyCtrl.keyUp(key);
		}


		if (istouch()) {
			if (!touch_pressed) {
				// first touch
				touch.touchBegin(touchx(), touchy());
			}else {
				if (istouch_moved()) {
					// move
					touch.touchMoved(touchx(), touchy());
				}
			}
			touch_pressed = true;
		}else {
			// end touch
			if (touch_pressed) {
				if (isotouch()) {
					touch.touchEnded(otouchx(), otouchy());
				}else {
					touch.touchEnded(0, 0);
				}
			}
			touch_pressed = false;
		}
	}
} InputContent;

InputContent input;

void initGame() {
	vita2d_init();
	vita2d_set_clear_color(BLACK);

	pgf = vita2d_load_default_pgf();
	memset(&input, 0, sizeof(input));

    sceCtrlSetSamplingMode(SCE_CTRL_MODE_ANALOG_WIDE);
    sceTouchSetSamplingState(SCE_TOUCH_PORT_FRONT, SCE_TOUCH_SAMPLING_STATE_START);
    sceTouchSetSamplingState(SCE_TOUCH_PORT_BACK, SCE_TOUCH_SAMPLING_STATE_START);

    srand(time(NULL));
}

void termGame() {
	vita2d_free_pgf(pgf);
	vita2d_fini();
}

void exitApp() {
	exitVal = true;
}

void showFPS(bool b) {
	showFps = b;
}

void setScreenColor(unsigned int color) {
	vita2d_set_clear_color(color);
}


typedef struct DebugTextContent {
	std::string text;
	int color;
} DebugTextContent;
std::vector<DebugTextContent> debug_text_arr;

void add_debug_text(int color, char const* text, ...) {
	char buf[1024];
	va_list argptr;
	va_start(argptr, text);
	vsnprintf(buf, sizeof(buf), text, argptr);
	va_end(argptr);
	DebugTextContent content = {buf,color};
	debug_text_arr.push_back(content);
}

void draw_debug_text() {
	int size = debug_text_arr.size();
	int index = size <= 27 ? 0 : size - 28;
	int y = size <= 27 ? 20 : (544-20*27);
	for (int i = index; i < size; ++i) {
		vita2d_pgf_draw_textf(pgf, 0, y, debug_text_arr[i].color, 1.0f, debug_text_arr[i].text.c_str());
		y += 20;
	}
}

void clear_debug_text() {
	debug_text_arr.clear();
}

int Run(int argc, char const* argv[], App* app) {
	initGame();

	float dt = 0.0f;
	SceUInt64 cur_micros = 0, delta_micros = 0, last_micros = 0;
	last_micros = sceKernelGetProcessTimeWide();

	lite2d::AppContent& content = lite2d::AppContent::instance();

	app->onAppStart();
	while(!exitVal) {

		// input
		input.InputProc();

		cur_micros = sceKernelGetProcessTimeWide();

		// update
		content.getDispatcherHolder().update(dt);

	    // display begin
		vita2d_start_drawing();
		vita2d_clear_screen();

		//vita2d_set_region_clip(SCE_GXM_REGION_CLIP_ALL, 300, 50, 960-300, 544-50);

		content.getRootWindow().render();

		draw_debug_text();

		if (showFps) {
			vita2d_pgf_draw_textf(pgf, 840, 20, WHITE, 1.0f, "FPS %.2f", (1.0f / dt));
		}

	    // display end
	    vita2d_end_drawing();
		vita2d_swap_buffers();

	    delta_micros = cur_micros - last_micros;
		last_micros = cur_micros;
		dt = (float)delta_micros / 1000000.0f;
	}
	app->onAppStop();

	lite2d::AppContent::destory();
	lite2d::destoryRender();

	termGame();
	sceKernelExitProcess(0);
	return 0;
}

bool registerProxy(Proxy* proxy) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	return content.getProxyHolder().registerProxy(proxy);
}

bool removeProxy(std::string const& proxyName) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	return content.getProxyHolder().removeProxy(proxyName);
}

Proxy& getProxy(std::string const& proxyName) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	return content.getProxyHolder().getProxy(proxyName);
}

Node& getRootWindow() {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	return content.getRootWindow();
}

bool registerWorker(Worker* worker) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	return content.getWorkerHolder().registerWorker(worker);
}

bool removeWorker(std::string const& workerName) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	return content.getWorkerHolder().removeWorker(workerName);
}

Worker& getWorker(std::string const& workerName) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	return content.getWorkerHolder().getWorker(workerName);
}

bool registerHandler(std::string const& eventName, HandlerImp* handler) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	return content.getHandlerHolder().registerHandler(eventName, handler);
}

bool removeHandler(std::string const& eventName, void* target) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	return content.getHandlerHolder().removeHandler(eventName, target);
}

void notify(Event const& args) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	return content.getHandlerHolder().notify(args);
}

void registerTexture(std::string const& fileName, unsigned char* buffer, unsigned int bufSize) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	content.getTextureCache().registerTexture(fileName, buffer, bufSize);
}

void registerTexture(std::string const& fileName, Texture* tex) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	content.getTextureCache().registerTexture(fileName, tex);
}

void removeTexture(std::string const& fileName) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	content.getTextureCache().removeTexture(fileName);
}

void removeUnusedTex() {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	content.getTextureCache().removeUnusedTex();
}

bool hasTexture(std::string const& fileName) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	return content.getTextureCache().hasTexture(fileName);
}

Texture* getTexture(std::string const& fileName) {
	lite2d::AppContent& content = lite2d::AppContent::instance();
	return content.getTextureCache().getTexture(fileName);
}

} // lite2d
