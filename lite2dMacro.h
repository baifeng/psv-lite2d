
#ifndef LITE2D_MACRO_H
#define LITE2D_MACRO_H

#include "stdio.h"
#include "functional"

#define SAFE_DELETE(p) if (p!=NULL) { \
	delete p; \
	p = NULL; \
}

#define SAFE_RELEASE(p) if (p!=NULL) { \
	p->release(); \
	p = NULL; \
}

#define SAFE_RETAIN(p) if (p!=NULL) {p->retain();}

#define RGBA8(r,g,b,a) ((((a)&0xFF)<<24) | (((b)&0xFF)<<16) | (((g)&0xFF)<<8) | (((r)&0xFF)<<0))

#define BLACK   RGBA8(  0,   0,   0, 255)
#define WHITE   RGBA8(255, 255, 255, 255)
#define GREEN   RGBA8(  0, 255,   0, 255)
#define RED     RGBA8(255,   0,   0, 255)
#define BLUE    RGBA8(  0,   0, 255, 255)
#define PURPLE  RGBA8(255,   0, 255, 255)

#define STDBIND(f, p) std::bind(f, p)
#define STDBIND1(f, p) std::bind(f, p, std::placeholders::_1)
#define STDBIND2(f, p) std::bind(f, p, std::placeholders::_1, std::placeholders::_2)
#define STDBIND3(f, p) std::bind(f, p, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)

#define STDBINDF(f) std::bind(f)
#define STDBINDF1(f) std::bind(f, std::placeholders::_1)
#define STDBINDF2(f) std::bind(f, std::placeholders::_1, std::placeholders::_2)
#define STDBINDF3(f) std::bind(f, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)

#endif // LITE2D_MACRO_H
