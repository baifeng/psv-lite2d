
#include "lite2dProxy.h"

namespace lite2d {

ProxyHolder::ProxyHolder() {

}

ProxyHolder::~ProxyHolder() {
	for (ProxyMap::iterator iter = this->mProxyMap.begin(); iter != this->mProxyMap.end(); ++iter) {
        delete iter->second;
    }
	this->mProxyMap.clear();
}

bool ProxyHolder::registerProxy(Proxy* proxy) {
	if (this->mProxyMap[proxy->getProxyName()]!=NULL) {
		delete proxy;
		return false;
	}
	this->mProxyMap[proxy->getProxyName()] = proxy;
	proxy->onAttach();
	return true;
}

bool ProxyHolder::removeProxy(std::string const& proxyName) {
	Proxy* proxy = this->mProxyMap[proxyName];
	if (proxy != NULL) {
		proxy->onDetach();
		delete proxy;
		this->mProxyMap.erase(proxyName);
		return true;
	}
	return false;
}

Proxy& ProxyHolder::getProxy(std::string const& proxyName) {
	return *this->mProxyMap[proxyName];
}

}
