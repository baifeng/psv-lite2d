
#ifndef VITA_RENDER_H
#define VITA_RENDER_H

#include "lite2dRender.h"
#include "lite2dStruct.h"
#include "vector"

class VitaRender : public lite2d::Render {
public:
	typedef std::vector<lite2d::Rect> ClipStack;
public:
	VitaRender();
public:
	virtual void beginClip(int x, int y, int w, int h);
	virtual void endClip();
public:
	virtual void drawpixel(float x, float y, unsigned int color);
	virtual void drawline(float x0, float y0, float x1, float y1, unsigned int color);
	virtual void drawrect(float x, float y, float w, float h, unsigned int color);
	virtual void drawcircle(float x, float y, float radius, unsigned int color);
public:
	virtual void draw(lite2d::Texture* texture, float x, float y);
	virtual void draw(lite2d::Texture* texture, float x, float y, unsigned int w, unsigned h);
public:
	virtual void drawtext(lite2d::Font* font, float x, float y, char const* text, ...);
private:
	ClipStack mClipStack;
};

#endif // VITA_RENDER_H
