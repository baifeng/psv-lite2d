
#ifndef LITE2D_UI_H
#define LITE2D_UI_H

#include "lite2dNode.h"
#include "string"

namespace lite2d {

class Texture;
class Sprite : public Node {
public:
	Sprite();
	virtual ~Sprite();
public:
	bool loadTexture(std::string const& fileName);
	void setTexture(Texture* tex);
protected:
	//virtual void onUpdate(float dt);
	virtual void onRender();
private:
	Texture* mpTex;
};

class Font;
class Label : public Node {
public:
	Label();
	~Label();
public:
	void setFontScale(float scale);
	void setTextColor(unsigned int color);
	void setText(std::string const& text);
public:
	float getFontScale() const;
	unsigned int getTextColor() const;
	std::string const& getText() const {
		return this->mText;
	}
private:
	virtual void onRender();
private:
	Font* mpFont;
	std::string mText;
};

class Button : public Node {
public:
	typedef std::function<void(Node*)> TouchFunc;
	typedef enum {
		STATE_NORMAL = 0,
		STATE_HIGHLIGHT = 1,
		STATE_DISABLED = 2,
		STATE_UNKNOWN = 255,
	} State;
public:
	Button();
	~Button();
public:
	void setTitleColor(unsigned int color, State kState);
	void setTitle(std::string const& title, State kState);
	void setImage(Texture* tex, State kState);
public:
	unsigned int titleColorWithState(State kState) const;
	std::string const& titleWithState(State kState) const;
	Texture* imageWithState(State kState) const;
public:
	void setEnable(bool b);
	bool isEnable() const;
public:
	void setDownFunc(TouchFunc func);
	void setUpFunc(TouchFunc func);
public:
	Label* getTitleView() const;
private:
	void setState(State kState);
private:
	virtual void onAttach();
	virtual void onDetach();
private:
	virtual void onRender();
private:
	virtual bool onTouchBegin(int x, int y);
	virtual void onTouchMoved(int x, int y);
	virtual void onTouchEnded(int x, int y);
private:
	State mkCurState;
	Label* mpTitle;
	Texture* mpTex[3];
	unsigned int mColor[3];
	std::string mText[3];
	TouchFunc mDownFunc;
	TouchFunc mUpFunc;
};

}

#endif // LITE2D_SPRITE_H
