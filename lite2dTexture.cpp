
#include "lite2dTexture.h"
#include "lite2dMacro.h"
#include "vita2d.h"

namespace lite2d {

Texture::Texture(void* texture) {
	this->texture = texture;
}

Texture::~Texture() {
	this->freeTexture();
}

void Texture::freeTexture() {
	if (this->texture != NULL) {
		vita2d_free_texture((vita2d_texture*)this->texture);
		this->texture = NULL;
	}
}

void* Texture::getTexture() const {
	return this->texture;
}

int Texture::getWidth() const {
	if (this->texture) {
		return vita2d_texture_get_width((vita2d_texture*)this->texture);
	}
	return 0;
}

int Texture::getHeight() const {
	if (this->texture) {
		return vita2d_texture_get_height((vita2d_texture*)this->texture);
	}
	return 0;
}


TextureCache::TextureCache() {

}

TextureCache::~TextureCache() {
	for (TextureMap::iterator i = this->mTexMap.begin(); i != this->mTexMap.end(); ++i) {
		SAFE_RELEASE(this->mTexMap[i->first]);
	}
	this->mTexMap.clear();
}

void TextureCache::registerTexture(std::string const& fileName, unsigned char* buffer, unsigned int bufSize) {
	Texture* tex = new lite2d::Texture(vita2d_load_PNG_buffer(buffer));
	this->registerTexture(fileName, tex);
	SAFE_RELEASE(tex);
}

void TextureCache::registerTexture(std::string const& fileName, Texture* tex) {
	if (this->mTexMap[fileName] == NULL) {
		this->mTexMap[fileName] = tex;
		SAFE_RETAIN(tex);
	}
}

void TextureCache::removeTexture(std::string const& fileName) {
	SAFE_RELEASE(this->mTexMap[fileName]);
	this->mTexMap.erase(fileName);
}

void TextureCache::removeUnusedTex() {
	for (TextureMap::iterator i = this->mTexMap.begin(); i != this->mTexMap.end();) {
		if (i->second == NULL) {
			this->mTexMap.erase(i++);
		}else {
			if (i->second->refCount()==1) {
				i->second->release();
				this->mTexMap.erase(i++);
			}else {
				++i;
			}
		}
	}
}

bool TextureCache::hasTexture(std::string const& fileName) {
	return this->mTexMap[fileName] != NULL;
}

Texture* TextureCache::getTexture(std::string const& fileName) {
	return this->mTexMap[fileName];
}

} // lite2d
