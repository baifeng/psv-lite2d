
#include "MyApp.h"
#include "LandMine.h"
#include "GameMenu.h"
#include "lite2d.h"

void removeNode(lite2d::Node* sender) {
    //sender->removeFromParent();
    //delete sender;
}

extern unsigned char _binary_assets_img_bg_test_png_start;

extern unsigned char _binary_assets_img_tile_base_png_start;
extern unsigned char _binary_assets_img_tile_down_png_start;
extern unsigned char _binary_assets_img_tile_mask_png_start;

extern unsigned char _binary_assets_img_smile1_down_png_start;
extern unsigned char _binary_assets_img_smile1_png_start;
extern unsigned char _binary_assets_img_smile2_down_png_start;
extern unsigned char _binary_assets_img_smile2_png_start;
extern unsigned char _binary_assets_img_smile3_down_png_start;
extern unsigned char _binary_assets_img_smile3_png_start;

extern unsigned char _binary_assets_img_option_down_png_start;
extern unsigned char _binary_assets_img_option_png_start;

extern unsigned char _binary_assets_img_number_0_png_start;
extern unsigned char _binary_assets_img_number_1_png_start;
extern unsigned char _binary_assets_img_number_2_png_start;
extern unsigned char _binary_assets_img_number_3_png_start;
extern unsigned char _binary_assets_img_number_4_png_start;
extern unsigned char _binary_assets_img_number_5_png_start;
extern unsigned char _binary_assets_img_number_6_png_start;
extern unsigned char _binary_assets_img_number_7_png_start;
extern unsigned char _binary_assets_img_number_8_png_start;
extern unsigned char _binary_assets_img_number_9_png_start;
extern unsigned char _binary_assets_img_number_dash_png_start;

void MyApp::onAppStart() {
    // 显示fps
    //lite2d::showFPS(true);
    lite2d::setScreenColor(RGBA8(192, 192, 192, 255));

    // 加载图片
    lite2d::registerTexture("bg_test.png", &_binary_assets_img_bg_test_png_start, 0);
    lite2d::registerTexture("tile_base.png", &_binary_assets_img_tile_base_png_start, 0);
    lite2d::registerTexture("tile_down.png", &_binary_assets_img_tile_down_png_start, 0);
    lite2d::registerTexture("tile_mask.png", &_binary_assets_img_tile_mask_png_start, 0);

    lite2d::registerTexture("smile1_down.png", &_binary_assets_img_smile1_down_png_start, 0);
    lite2d::registerTexture("smile1.png", &_binary_assets_img_smile1_png_start, 0);
    lite2d::registerTexture("smile2_down.png", &_binary_assets_img_smile2_down_png_start, 0);
    lite2d::registerTexture("smile2.png", &_binary_assets_img_smile2_png_start, 0);
    lite2d::registerTexture("smile3_down.png", &_binary_assets_img_smile3_down_png_start, 0);
    lite2d::registerTexture("smile3.png", &_binary_assets_img_smile3_png_start, 0);

    lite2d::registerTexture("option_down.png", &_binary_assets_img_option_down_png_start, 0);
    lite2d::registerTexture("option.png", &_binary_assets_img_option_png_start, 0);

    lite2d::registerTexture("number_0.png", &_binary_assets_img_number_0_png_start, 0);
    lite2d::registerTexture("number_1.png", &_binary_assets_img_number_1_png_start, 0);
    lite2d::registerTexture("number_2.png", &_binary_assets_img_number_2_png_start, 0);
    lite2d::registerTexture("number_3.png", &_binary_assets_img_number_3_png_start, 0);
    lite2d::registerTexture("number_4.png", &_binary_assets_img_number_4_png_start, 0);
    lite2d::registerTexture("number_5.png", &_binary_assets_img_number_5_png_start, 0);
    lite2d::registerTexture("number_6.png", &_binary_assets_img_number_6_png_start, 0);
    lite2d::registerTexture("number_7.png", &_binary_assets_img_number_7_png_start, 0);
    lite2d::registerTexture("number_8.png", &_binary_assets_img_number_8_png_start, 0);
    lite2d::registerTexture("number_9.png", &_binary_assets_img_number_9_png_start, 0);
    lite2d::registerTexture("number_dash.png", &_binary_assets_img_number_dash_png_start, 0);

    // 注册数据类
    lite2d::registerProxy(new TileMapProxy(TileMapProxy::ProxyName));
    
    // 获取数据类
    //TileMapProxy& map = (TileMapProxy&)lite2d::getProxy(TileMapProxy::ProxyName);
    //map.start(10, 10, 32);
    
    // 视图
    GameStateView* gameView = new GameStateView();
    lite2d::getRootWindow().addChild(gameView);
}

void MyApp::onAppStop() {
    lite2d::removeTexture("bg_test.png");
    lite2d::removeTexture("tile_base.png");
    lite2d::removeTexture("tile_down.png");
    lite2d::removeTexture("tile_mask.png");

    lite2d::removeTexture("smile1_down.png");
    lite2d::removeTexture("smile1.png");
    lite2d::removeTexture("smile2_down.png");
    lite2d::removeTexture("smile2.png");
    lite2d::removeTexture("smile3_down.png");
    lite2d::removeTexture("smile3.png");

    lite2d::removeTexture("option_down.png");
    lite2d::removeTexture("option.png");

    lite2d::removeTexture("number_0.png");
    lite2d::removeTexture("number_1.png");
    lite2d::removeTexture("number_2.png");
    lite2d::removeTexture("number_3.png");
    lite2d::removeTexture("number_4.png");
    lite2d::removeTexture("number_5.png");
    lite2d::removeTexture("number_6.png");
    lite2d::removeTexture("number_7.png");
    lite2d::removeTexture("number_8.png");
    lite2d::removeTexture("number_9.png");
    lite2d::removeTexture("number_dash.png");
}
