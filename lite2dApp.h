#ifndef LITE2D_APP_H
#define LITE2D_APP_H

#include "stdio.h"
#include "string"

namespace lite2d {

class App {
public:
	virtual ~App() {}
public:
	virtual void onAppStart() {}
	virtual void onAppStop() {}
	virtual void onAppWakeup() {}
	virtual void onAppSleep() {}
};

// main
int Run(int argc, char const* argv[], App* app);

template<typename T>
int Run(int argc, char const* argv[]) {
	T app;
	return Run(argc, argv, &app);
}

// model
class Proxy;
bool registerProxy(Proxy* proxy);
bool removeProxy(std::string const& proxyName);
Proxy& getProxy(std::string const& proxyName);

// view
class Node;
Node& getRootWindow();

// controller
class Worker;
bool registerWorker(Worker* worker);
bool removeWorker(std::string const& workerName);
Worker& getWorker(std::string const& workerName);

// notification
class Event;
class HandlerImp;
bool registerHandler(std::string const& eventName, HandlerImp* handler);
bool removeHandler(std::string const& eventName, void* target);
void notify(Event const& args);

// texture cache
class Texture;
void registerTexture(std::string const& fileName, unsigned char* buffer, unsigned int bufSize);
void registerTexture(std::string const& fileName, Texture* tex);
void removeTexture(std::string const& fileName);
void removeUnusedTex();
bool hasTexture(std::string const& fileName);
Texture* getTexture(std::string const& fileName);

// tools
void exitApp();
void showFPS(bool b);
void setScreenColor(unsigned int color);

void add_debug_text(int color, char const* text, ...);
void clear_debug_text();

} // lite2d

#endif // LITE2D_APP_H
