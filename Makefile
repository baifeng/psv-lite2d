TITLE_ID = BTLSHPARN
TARGET   = Lite2D
ASSETS	 = assets/gui/background.o assets/gui/field.o assets/gui/items.o \
			assets/img/tile_1.o assets/img/tile_2.o assets/img/tile_3.o \
			assets/img/tile_4.o assets/img/tile_5.o assets/img/tile_6.o \
			assets/img/tile_7.o assets/img/tile_8.o \
			assets/img/tile_b.o assets/img/tile_b2.o assets/img/tile_b3.o \
			assets/img/tile_d.o assets/img/tile_f.o \
			assets/img/tile_base.o assets/img/tile_down.o assets/img/tile_mask.o \
			assets/img/smile1.o assets/img/smile1_down.o \
			assets/img/smile2.o assets/img/smile2_down.o \
			assets/img/smile3.o assets/img/smile3_down.o \
			assets/img/option.o assets/img/option_down.o \
			assets/img/number_0.o assets/img/number_1.o assets/img/number_2.o \
			assets/img/number_3.o assets/img/number_4.o assets/img/number_5.o \
			assets/img/number_6.o assets/img/number_7.o assets/img/number_8.o \
			assets/img/number_9.o assets/img/number_dash.o \
			assets/img/bg_test.o
OBJS     = main.o Field.o lite2dObject.o lite2dStruct.o lite2dNode.o \
			lite2dApp.o lite2dAppContent.o lite2dUI.o lite2dEvent.o \
			lite2dProxy.o lite2dWorker.o lite2dDispatch.o lite2dResponder.o \
			lite2dTexture.o lite2dFont.o VitaRender.o \
			LandMine.o MyApp.o GameView.o GameMenu.o
OBJS    += $(ASSETS)

LIBS = -lvita2d -lSceDisplay_stub -lSceGxm_stub \
	-lSceSysmodule_stub -lSceCtrl_stub -lSceTouch_stub -lScePgf_stub \
	-lSceCommonDialog_stub -lfreetype -lpng -ljpeg -lz -lm -lc

PREFIX  = arm-vita-eabi
CC      = $(PREFIX)-gcc
CFLAGS  = -Wl,-q -Wall -O3

CXX 	  = $(PREFIX)-g++
CXXFLAGS  = -Wl,-q -Wall -O3 -std=c++0x

ASFLAGS = $(CXXFLAGS)

all: $(TARGET).vpk

%.vpk: eboot.bin
	vita-mksfoex -s TITLE_ID=$(TITLE_ID) "$(TARGET)" param.sfo
	vita-pack-vpk -s param.sfo -b eboot.bin \
		--add sce_sys/icon0.png=sce_sys/icon0.png \
		--add sce_sys/livearea/contents/bg.png=sce_sys/livearea/contents/bg.png \
		--add sce_sys/livearea/contents/startup.png=sce_sys/livearea/contents/startup.png \
		--add sce_sys/livearea/contents/template.xml=sce_sys/livearea/contents/template.xml \
	 $@

eboot.bin: $(TARGET).velf
	vita-make-fself $< $@

%.velf: %.elf
	vita-elf-create $< $@

$(TARGET).elf: $(OBJS)
	$(CXX) $(CXXFLAGS) $^ $(LIBS) -o $@

%.o: %.png
	$(PREFIX)-ld -r -b binary -o $@ $^
	
%.o : %.cpp
	$(CXX) -c $(CXXFLAGS) -o $@ $<

clean:
	@rm -rf $(TARGET).vpk $(TARGET).velf $(TARGET).elf $(OBJS) \
		eboot.bin param.sfo

vpksend: $(TARGET).vpk
	curl -T $(TARGET).vpk ftp://$(PSVITAIP):1337/ux0:/
	@echo "Sent."

send: eboot.bin
	curl -T eboot.bin ftp://$(PSVITAIP):1337/ux0:/app/$(TITLE_ID)/
	@echo "Sent."
