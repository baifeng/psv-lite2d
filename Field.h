#ifndef _FIELD_H_
#define _FIELD_H_

#define FIELD_SIZE 10

// reserved
#define CELL_MISS 0xff
#define CELL_BOOM 0xfe

enum ShipDirection
{
	SHIP_DIR_VERTICAL = 0x00,
	SHIP_DIR_HORIZONTAL = 0x01
};

class Field
{
private:
	unsigned char _field[FIELD_SIZE][FIELD_SIZE];
public:
	Field();

	void fill(unsigned char value);
	void generate(unsigned char shipMaxLength);

	bool isTrueShipPlace(unsigned char x, unsigned char y, unsigned char length, ShipDirection direction);
	void placeShip(unsigned char x, unsigned char y, unsigned char length, ShipDirection direction);
	void deleteShips(unsigned char type);

	unsigned char getCellValue(unsigned char x, unsigned char y);

	bool isAttackedCell(unsigned char x, unsigned char y);
	bool isCrushCell(unsigned char x, unsigned char y);
	bool attackCell(unsigned char x, unsigned char y);

	unsigned int unbrokenCellsCount();

};

#endif //_FIELD_H_