
#include "lite2dAppContent.h"
#include "lite2dMacro.h"
#include "lite2dNode.h"

#include "lite2dEvent.h"
#include "lite2dProxy.h"
#include "lite2dWorker.h"
#include "lite2dDispatch.h"

#include "lite2dTexture.h"

namespace lite2d {

static AppContent* content = NULL;

AppContent& AppContent::instance() {
	if (content==NULL) {
		content = new AppContent();
	}
	return *content;
}

void AppContent::destory() {
	SAFE_DELETE(content);
}

AppContent::AppContent() {
	this->mpDispatcherHolder = new DispatcherHolder();
	this->mpKeyCtrlHolder = new KeyCtrlHolder();
	this->mpTouchHolder = new TouchHolder();
	this->mpHandlerHolder = new HandlerHolder();
	this->mpProxyHolder = new ProxyHolder();
	this->mpWorkerHolder = new WorkerHolder();
	this->mpRootWindow = new Node();
	this->mpTextureCache = new TextureCache();
}

AppContent::~AppContent() {
	SAFE_DELETE(this->mpTextureCache);
	SAFE_DELETE(this->mpRootWindow);
	SAFE_DELETE(this->mpWorkerHolder);
	SAFE_DELETE(this->mpProxyHolder);
	SAFE_DELETE(this->mpHandlerHolder);
	SAFE_DELETE(this->mpTouchHolder);
	SAFE_DELETE(this->mpKeyCtrlHolder);
	SAFE_DELETE(this->mpDispatcherHolder);
}

Node& AppContent::getRootWindow() {
	return *this->mpRootWindow;
}

HandlerHolder& AppContent::getHandlerHolder() {
	return *this->mpHandlerHolder;
}

ProxyHolder& AppContent::getProxyHolder() {
	return *this->mpProxyHolder;
}

WorkerHolder& AppContent::getWorkerHolder() {
	return *this->mpWorkerHolder;
}

DispatcherHolder& AppContent::getDispatcherHolder() {
	return *this->mpDispatcherHolder;
}

TouchHolder& AppContent::getTouchHolder() {
	return *this->mpTouchHolder;
}

KeyCtrlHolder& AppContent::getKeyCtrlHolder() {
	return *this->mpKeyCtrlHolder;
}

TextureCache& AppContent::getTextureCache() {
	return *this->mpTextureCache;
}

}