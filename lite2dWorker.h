
#ifndef LITE2D_WORKER_H
#define LITE2D_WORKER_H

#include "lite2dEvent.h"
#include "lite2dAppContent.h"
#include "vector"

namespace lite2d {

class Worker {
public:
	typedef std::vector<std::string> WorkList;
public:
	Worker(std::string const& name) {
		this->mWorkerName = name;
		this->mpHandlerHolder = &AppContent::instance().getHandlerHolder();
	}
	virtual ~Worker() {
		this->mpHandlerHolder = NULL;
	}
public:
	std::string const& getWorkerName() const {
		return this->mWorkerName;
	}
	void notify(Event const& args) {
		this->mpHandlerHolder->notify(args);
	}
public:
	virtual void onAttach(){}
	virtual void onDetach(){}
	virtual void handle(Event const& args){}
	virtual WorkList worklist() {
		WorkList list;
		return list;
	}
private:
	HandlerHolder* mpHandlerHolder;
	std::string mWorkerName;
};

class WorkerHolder {
public:
	typedef std::map<std::string, Worker*> WorkerMap;
public:
	WorkerHolder();
	~WorkerHolder();
public:
	bool registerWorker(Worker* worker);
	bool removeWorker(std::string const& workerName);
	Worker& getWorker(std::string const& workerName);
private:
	WorkerMap mWorkerMap;
};

}

#endif // LITE2D_WORKER_H
