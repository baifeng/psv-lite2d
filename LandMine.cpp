
#include "LandMine.h"
#include "lite2d.h"
#include "map"

static lite2d::Point TileOffset[8] = {
	lite2d::makePoint(-1, -1),
	lite2d::makePoint(0, -1),
	lite2d::makePoint(+1, -1),
	lite2d::makePoint(-1, 0),
	lite2d::makePoint(+1, 0),
	lite2d::makePoint(-1, +1),
	lite2d::makePoint(0, +1),
	lite2d::makePoint(+1, +1),
};

static int makePosKey(int x, int y) {
	return (y << 16) + x;
}

static int getMineNumber(std::map<int, bool>& mineMap, int x, int y) {
	int num = 0;
	for (int i = 0; i < 8; ++i) {
		int x0 = x + TileOffset[i].x;
		int y0 = y + TileOffset[i].y;
		if (mineMap[ makePosKey(x0, y0) ]) {
			num ++;
		}
	}
	return num;
}

Shuffle::Shuffle() {

}

Shuffle::~Shuffle() {

}

void Shuffle::input(unsigned char width, unsigned char height, unsigned short number) {
	this->mTileArr.reserve(width * height);
	this->mTileArr.clear();
	Tile tile;
	// 创建元素
	for (int h = 0; h < height; ++h) {
		for (int w = 0; w < width; ++w) {
			tile.x = w;
			tile.y = h;
			if (this->mTileArr.size() < number) {
				tile.tileId = TILE_TYPE_MINE;
			}else {
				tile.tileId = TILE_TYPE_NONE;
			}
			this->mTileArr.push_back(tile);
		}
	}
	// 打乱地图
	for (int i = 0; i < (int)this->mTileArr.size(); ++i) {
		int index = rand() % this->mTileArr.size();
		if (index == i) {
			continue;
		}
		TileType tileId = this->mTileArr[i].tileId;
		this->mTileArr[i].tileId = this->mTileArr[index].tileId;
		this->mTileArr[index].tileId = tileId;
	}
	typedef std::map<int, bool> MineMap;
	MineMap mineMap;
	// 记录地雷位置
	for (int i = 0; i < (int)this->mTileArr.size(); ++i) {
		Tile const& tile = this->mTileArr[i];
		if (tile.tileId == TILE_TYPE_MINE) {
			mineMap[ makePosKey(tile.x, tile.y) ] = true;
		}
	}
	// 计算数字
	for (int i = 0; i < (int)this->mTileArr.size(); ++i) {
		Tile& tile = this->mTileArr[i];
		if (tile.tileId == TILE_TYPE_MINE) {
			continue;
		}
		int num = getMineNumber(mineMap, tile.x, tile.y);
		if (num > 0) {
			tile.tileId = TILE_TYPE_NUMBER;
			tile.tileValue = num;
		}
	}
}

void Shuffle::output(TileMapProxy* proxy) {
	TileMapProxy& tileMap = *proxy;
	tileMap.getMineArray().clear();
	// 地图内容输出到数据类
	for (int i = 0; i < (int)this->mTileArr.size(); ++i) {
		Tile const& tile = this->mTileArr[i];
		if (tile.tileId == TILE_TYPE_NONE) {
			continue;
		}
		TileData& data = tileMap[tile.y][tile.x];
		data.tileId = tile.tileId;
		if (tile.tileId == TILE_TYPE_NUMBER) {
			data.tileValue = tile.tileValue;
		}
		if (tile.tileId == TILE_TYPE_MINE) {
			tileMap.getMineArray().push_back(lite2d::makePoint(tile.x, tile.y));
		}
	}
}


EmptyTileOpen::EmptyTileOpen() {
	this->mResultList.reserve(1024);
}

EmptyTileOpen::~EmptyTileOpen() {
	this->mResultList.clear();
}

void EmptyTileOpen::searchEmptyTile(TileMapProxy* proxy, lite2d::Point const& firstPt) {
	typedef std::map<int, bool> TileFlag;

	this->mResultList.clear();
	this->miValidCount = 0;

	TileList list1;
	list1.reserve(1024);
	list1.push_back(firstPt);

	TileList& list2 = this->mResultList;
	TileMapProxy& tileMap = *proxy;

	TileFlag flag1;
	TileFlag flag2;

	do {
		// 把待检查列表的一个元素移入已检查列表
		list2.push_back( list1.back() );
		list1.pop_back();

		lite2d::Point const& pt = list2.back();

		if (!tileMap.isOpen(pt.x, pt.y)) {
			// 只有未打开的方块才加入计数
			this->miValidCount ++;
		}

		// 标记节点是否存在于列表
		flag1[ makePosKey(pt.x, pt.y) ] = false;
		flag2[ makePosKey(pt.x, pt.y) ] = true;

		// 如果移入的节点是个无雷块, 那么把该无雷块一圈的节点移入待检查列表
		if (tileMap.isEmptyTile(pt.x, pt.y)) {
			for (int i = 0; i < 8; ++i) {
				int x = pt.x + TileOffset[i].x;
				int y = pt.y + TileOffset[i].y;
				if (tileMap.isValidCoord(x, y) && !flag1[ makePosKey(x, y) ] && !flag2[ makePosKey(x, y) ]) {
					// 如果坐标有效不越界, 并且节点没有被记录, 把节点加入待检查列表
					list1.push_back(lite2d::makePoint(x, y));
					flag1[ makePosKey(x, y) ] = true;
				}
			}
		}

	} while (list1.size() > 0);
}

void EmptyTileOpen::openEmptyTile(TileMapProxy* proxy) {
	for (int i = 0; i < (int)this->mResultList.size(); ++i) {
		lite2d::Point const& pt = this->mResultList[i];
		(*proxy)[pt.y][pt.x].tileState = TILE_STATE_OPEN;
	}
}


std::string const TileMapProxy::ProxyName = "TileMapProxy";
std::string const TileMapProxy::GameStartEvent = "TileMapProxy::GameStartEvent";
std::string const TileMapProxy::GameOverEvent = "TileMapProxy::GameOverEvent";
std::string const TileMapProxy::GameClearEvent = "TileMapProxy::GameClearEvent";
std::string const TileMapProxy::MineUpdateEvent = "TileMapProxy::MineUpdateEvent";

void makeTileArraySize(TileMapProxy::TileArray& line, int size) {
	line.reserve(255);
	if ((int)line.size() < size) {
		for (int i = 0; i < size-(int)line.size(); ++i) {
			line.push_back(TileData());
		}
	}
}

TileMapProxy::TileMapProxy(std::string const& name):Proxy(name) {
	this->width = 0;
	this->height = 0;
	this->mTileMap.reserve(255);
	this->mMineArray.reserve(1024);
	//this->setMapInfo(255,255,0);
}

TileMapProxy::~TileMapProxy() {
	this->mTileMap.clear();
}

TileMapProxy::TileArray& TileMapProxy::operator[](unsigned char index) {
	if (index >= this->mTileMap.size()) {
		for (int i = 0; i < index-(int)this->mTileMap.size()+1; ++i) {
			TileArray arr;
			arr.reserve(255);
			this->mTileMap.push_back(arr);
		}
	}
	return this->mTileMap[index];
}

TileMapProxy::TileMineArray& TileMapProxy::getMineArray() {
	return this->mMineArray;
}

bool TileMapProxy::setMapInfo(unsigned char width, unsigned char height, unsigned short number) {
	if (number > width * height) {
		// 地雷数需要小于或等于地图容量
		return false;
	}

	this->width = width;
	this->height = height;
	this->mine_count = number;

	for (int h = 0; h < this->height; ++h) {
		for (int w = 0; w < this->width; ++w) {
			makeTileArraySize((*this)[h], this->width);
			(*this)[h][w].clear();
		}
	}

	// 初始化地雷
	Shuffle shuffle;
	shuffle.input(width, height, number);
	shuffle.output(this);

	return true;
}

void TileMapProxy::flagTile(int x, int y) {
	if (this->isOpen(x, y)) {
		return;
	}
	TileData& tile = (*this)[y][x];
	if (tile.tileTag != TILE_TAG_FLAG) {
		tile.tileTag = TILE_TAG_FLAG;
		this->miCurMineCount --;
	}else {
		tile.tileTag = TILE_TAG_NONE;
		this->miCurMineCount ++;
	}
	this->checkResult();

	if (this->miCurCloseCount > 0) {
		this->notify(lite2d::IntEvent(TileMapProxy::MineUpdateEvent, this->getCurMineCount()));
	}
}

void TileMapProxy::openTile(int x, int y) {

	if (this->gameover) {
		return;
	}

	TileData& tile = (*this)[y][x];
	if (tile.tileState != TILE_STATE_OPEN && tile.tileTag != TILE_TAG_FLAG) {
		
		if (this->isEmptyTile(x, y)) {
			// 如果是空白块,把附近的无雷区域全部打开
			EmptyTileOpen logic;
			logic.searchEmptyTile(this, lite2d::makePoint(x, y));
			logic.openEmptyTile(this);
			this->miCurCloseCount -= logic.getValidOpenCount();
			this->checkResult();
			return;
		}

		tile.tileState = TILE_STATE_OPEN;
		this->miCurCloseCount --;

		if (tile.tileId ==  TILE_TYPE_MINE) {
			this->gameover = true;
		}

		this->checkResult();
	}
}

void TileMapProxy::autoOpen(int x, int y) {
	if (!this->isNumberTile(x, y)) {
		return;
	}
	int num = (*this)[y][x].tileValue;

	if (num == 0) {
		return;
	}

	int curNum = 0;
	// 计算一圈的旗子数量
	for (int i = 0; i < 8; ++i) {
		int x0 = x + TileOffset[i].x;
		int y0 = y + TileOffset[i].y;
		if (!this->isValidCoord(x0, y0)) {
			continue;
		}
		if (!this->isOpen(x0, y0) && this->hasFlag(x0, y0)) {
			curNum ++;
		}
	}
	if (num == curNum) {
		// 如果旗子数量和地雷数一致, 自动打开剩余区域
		for (int i = 0; i < 8; ++i) {
			int x0 = x + TileOffset[i].x;
			int y0 = y + TileOffset[i].y;
			if (!this->isValidCoord(x0, y0)) {
				continue;
			}
			this->openTile(x0, y0);
		}
	}
}

void TileMapProxy::gameOver() {
	// 把剩下的所有地雷都标记出来
	for (int i = 0; i < (int)this->mMineArray.size(); ++i) {
		lite2d::Point const& pt = this->mMineArray[i];
		TileData& tile = (*this)[pt.y][pt.x];
		if (tile.tileTag != TILE_TAG_FLAG && tile.tileState != TILE_STATE_OPEN) {
			tile.tileState = TILE_STATE_SHOW_TIPS;
		}
	}
	this->notify(lite2d::Event(TileMapProxy::GameOverEvent));
}

void TileMapProxy::youWin() {
	// 把剩下的所有地雷都标记出来
	for (int i = 0; i < (int)this->mMineArray.size(); ++i) {
		lite2d::Point const& pt = this->mMineArray[i];
		TileData& tile = (*this)[pt.y][pt.x];
		tile.tileTag = TILE_TAG_FLAG;
	}

	this->miCurMineCount = 0;

	this->notify(lite2d::Event(TileMapProxy::GameClearEvent));
	this->notify(lite2d::IntEvent(TileMapProxy::MineUpdateEvent, this->getCurMineCount()));
}

void TileMapProxy::checkResult() {

	if (this->miCurCloseCount==0) {
		this->gameclear = true;
		this->youWin();
	}

	if (this->gameover) {
		this->gameOver();
		return;
	}
}

bool TileMapProxy::start(unsigned char width, unsigned char height, unsigned short number) {
	this->gameover = false;
	this->gameclear = false;
	this->miCurMineCount = number;
	this->miCurCloseCount = width * height - number;
	if (this->setMapInfo(width, height, number)) {
		this->notify(TileMapProxy::GameStartEvent);
	}
	return false;
}

bool TileMapProxy::isValidCoord(int x, int y) {
	if (x < 0 || x >= this->getWidth() || y < 0 || y >= this->getHeight()) {
		return false;
	}
	return true;
}

bool TileMapProxy::isEmptyTile(int x, int y) {
	TileData& tile = (*this)[y][x];
	return tile.tileId == TILE_TYPE_NONE;
}

bool TileMapProxy::isNumberTile(int x, int y) {
	TileData& tile = (*this)[y][x];
	return tile.tileId == TILE_TYPE_NUMBER;
}

bool TileMapProxy::isMineTile(int x, int y) {
	TileData& tile = (*this)[y][x];
	return tile.tileId == TILE_TYPE_MINE;
}

bool TileMapProxy::isOpen(int x, int y) {
	TileData& tile = (*this)[y][x];
	return tile.tileState == TILE_STATE_OPEN;
}

bool TileMapProxy::hasFlag(int x, int y) {
	TileData& tile = (*this)[y][x];
	return tile.tileTag == TILE_TAG_FLAG;
}
