
#ifndef LITE2D_RENDER_H
#define LITE2D_RENDER_H

#include "stdio.h"

namespace lite2d {

class Font;
class Texture;
class Render {
public:
	virtual ~Render(){}
public:
	virtual void beginClip(int x, int y, int w, int h){}
	virtual void endClip(){}
public:
	virtual void drawpixel(float x, float y, unsigned int color){}
	virtual void drawline(float x0, float y0, float x1, float y1, unsigned int color){}
	virtual void drawrect(float x, float y, float w, float h, unsigned int color){}
	virtual void drawcircle(float x, float y, float radius, unsigned int color){}
public:
	virtual void draw(Texture* texture, float x, float y){}
	virtual void draw(Texture* texture, float x, float y, unsigned int w, unsigned h){}
public:
	virtual void drawtext(Font* font, float x, float y, char const* text, ...){}
};

extern Render* sharedRender();
extern void destoryRender();

} // lite2d

#endif // LITE2D_RENDER_H
