
#include "lite2dStruct.h"

namespace lite2d {

Point makePoint(float x, float y) {
	Point point = {x, y};
	return point;
}

Size makeSize(float width, float height) {
	Size size = {width, height};
	return size;
}

Rect makeRect(float x, float y, float width, float height) {
	Rect rect = {x, y, width, height};
	return rect;
}

} // lite2d

