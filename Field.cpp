#include "Field.h"

#include <stdlib.h>
#include <time.h>

Field::Field()
{
	for (int i = 0; i < FIELD_SIZE; i++)
	{
		for (int j = 0; j < FIELD_SIZE; j++)
		{
			_field[i][j] = 0;
		}
	}
	//this->fill(0);
}

void Field::fill(unsigned char value)
{
	for (int i = 0; i < FIELD_SIZE; i++)
	{
		for (int j = 0; j < FIELD_SIZE; j++)
		{
			_field[i][j] = value;
		}
	}
}

void Field::generate(unsigned char shipMaxLength)
{
	unsigned char x, y, length;
	ShipDirection direction;
	unsigned char randDir, shipsCount;

	unsigned char trys;

reset:

	srand(time(NULL));
	this->fill(0);

	//Place ships
	length = shipMaxLength;
	shipsCount = 1;

	do
	{
		for (int i = 0; i < shipsCount; i++)
		{
			trys = 0;
			do
			{
				trys++;
				randDir = rand() % 2;
				if (randDir == 0)
				{
					direction = SHIP_DIR_VERTICAL;
					x = rand() % FIELD_SIZE;
					y = rand() % (FIELD_SIZE - length + 1);
				}
				else
				{
					direction = SHIP_DIR_HORIZONTAL;
					x = rand() % (FIELD_SIZE - length + 1);
					y = rand() % FIELD_SIZE;		
				}
				if (trys > 100) goto reset;
			}
			while (!this->isTrueShipPlace(x, y, length, direction));

			this->placeShip(x, y, length, direction);
		}
		length--;
		shipsCount++;		
	}
	while (length > 0);
}

bool Field::isTrueShipPlace(unsigned char x, unsigned char y, unsigned char length, ShipDirection direction)
{
	unsigned char a, b;

	switch (direction)
	{
	case SHIP_DIR_VERTICAL:
		if (y > FIELD_SIZE - length) return false;
		if (x > FIELD_SIZE - 1) return false;

		(y == 0) ? a = 0 : a = y - 1;
		(y == FIELD_SIZE - length) ? b = 9 : b = y + length;

		for (int i = a; i <= b; i++)
		{
			if (_field[x][i] != 0) return false;
			if (x > 0) if (_field[x - 1][i] != 0) return false;
			if (x < FIELD_SIZE - 1) if (_field[x + 1][i] != 0) return false;
		}
		break;

	case SHIP_DIR_HORIZONTAL:
		if (x > FIELD_SIZE - length) return false;
		if (y > FIELD_SIZE - 1) return false;

		(x == 0) ? a = 0 : a = x - 1;
		(x == FIELD_SIZE - length) ? b = 9 : b = x + length;

		for (int i = a; i <= b; i++)
		{
			if (_field[i][y] != 0) return false;
			if (y > 0) if (_field[i][y - 1] != 0) return false;
			if (y < FIELD_SIZE - 1) if (_field[i][y + 1] != 0) return false;
		}
		break;

	default:
		return false;
		break;
	}
	
	return true;
}

void Field::placeShip(unsigned char x, unsigned char y, unsigned char length, ShipDirection direction)
{
	switch (direction)
	{
	case SHIP_DIR_VERTICAL:
		for (int i = 0; i < length; i++)
		{
			_field[x][y + i] = length;
		}
		break;	

	case SHIP_DIR_HORIZONTAL:
		for (int i = 0; i < length; i++)
		{
			_field[x + i][y] = length;
		}
		break;

	default:
		return;
		break;
	}
}

void Field::deleteShips(unsigned char type)
{
	for (int i = 0; i < FIELD_SIZE; i++)
	{
		for (int j = 0; j < FIELD_SIZE; j++)
		{
			if (_field[i][j] == type) _field[i][j] = 0;
		}
	}
}

unsigned char Field::getCellValue(unsigned char x, unsigned char y)
{
	return _field[x][y];
}

bool Field::isAttackedCell(unsigned char x, unsigned char y)
{
	return (_field[x][y] == CELL_MISS) || (_field[x][y] == CELL_BOOM);
}

bool Field::isCrushCell(unsigned char x, unsigned char y)
{
	return (_field[x][y] == CELL_BOOM);
}

bool Field::attackCell(unsigned char x, unsigned char y)
{
	bool result = (_field[x][y] != 0);
	_field[x][y] = (result) ? CELL_BOOM : CELL_MISS;
	return result;
}

unsigned int Field::unbrokenCellsCount()
{
	unsigned int count = 0;

	for (int i = 0; i < FIELD_SIZE; i++)
	{
		for (int j = 0; j < FIELD_SIZE; j++)
		{
			if ((_field[i][j] != 0) && (!isAttackedCell(i, j))) count++;
		}
	}
	return count;	
}