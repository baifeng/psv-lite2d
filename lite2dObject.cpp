
#include "lite2dObject.h"

namespace lite2d {

Ref::Ref() {
	this->miRetainCount = 1;
}

Ref::~Ref() {

}

void Ref::retain() {
	this->miRetainCount++;
}

void Ref::release() {
	this->miRetainCount--;

	if (this->miRetainCount==0) {
		// other release

		// delete
		delete this;
	}
}


Object::~Object() {

}

} // lite2d
