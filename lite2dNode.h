
#ifndef LITE2D_NODE_H
#define LITE2D_NODE_H

#include <list>
#include <string>
#include "lite2dObject.h"
#include "lite2dStruct.h"
#include "lite2dResponder.h"
#include "lite2dDispatch.h"

namespace lite2d {

class Render;
class Node :
	public Object,
	public TouchResponder,
	public KeyResponder {
public:
	typedef std::list<Node*> ChildList;
public:
	Node();
	virtual ~Node();
public:
	void showBounds(bool b);
	void setNodeColor(unsigned int color); // can't rotate
public:
	void setPosition(float x, float y);
	Point const& getPosition() const;
	void setFrame(float x, float y, float width, float height);
	void setFrame(Rect const& frame);
	Rect const& getFrame() const;
	Point const& getWorldPos() const;
public:
	void setAnchor(Point const& anchor);
	Point const& getAnchor() const;
	void setScale(Point const& scale);
	Point const& getScale() const;
	void setAngle(float f);
	float getAngle() const;
public:
	void setVisible(bool b);
	bool isVisible() const;
	void setUpdateEnable(bool b);
	bool isUpdateEnable() const;
public:
	void setTag(int tag);
	int getTag() const;
public:
	bool addChild(Node* node);
	bool removeChild(Node* node);
	bool removeFromParent();
	Node* getChildWithTag(int tag);
	Node* getParent() const;
public:
	void setTouchEnable(bool b);
	virtual bool isTouchEnable() const;
	void setJoyBtnEnable(bool b);
	virtual bool isJoyBtnEnable() const;
	void setJoyAxisEnable(bool b);
	virtual bool isJoyAxisEnable() const;
public:
	virtual bool canBeTouch() const;
	virtual bool canBeHit(int local_x, int local_y) const;
	virtual bool is_hit(int local_x, int local_y) const;
	virtual void convertWorldPoint(int world_x, int world_y, int* local_x, int* local_y) const;
	virtual void convertLocalPoint(int local_x, int local_y, int* world_x, int* world_y) const;
public:
	void registerTouch();
	void removeTouch();
    void registerKeyCtrl();
    void removeKeyCtrl();
	void registerUpdate();
	void removeUpdate();
public:
	void update(float dt);
	void render();
public:
	void attach();
	void detach();
protected:
	void drawvline(Point const& offset, int length);
	void drawhline(Point const& offset, int length);
	void drawvline(Point const& offset, int length, unsigned int color);
	void drawhline(Point const& offset, int length, unsigned int color);
protected:
	virtual void onAttach();
	virtual void onDetach();
protected:
	virtual void onUpdate(float dt);
	virtual void onRender();
protected:
	virtual void onKeyDown(APP_CTRL_TYPE key);
	virtual void onKeyUp(APP_CTRL_TYPE key);
protected:
	virtual void onAxisMoveL(int x, int y);
	virtual void onAxisMoveR(int x, int y);
protected:
	virtual bool onTouchBegin(int x, int y);
	virtual void onTouchMoved(int x, int y);
	virtual void onTouchEnded(int x, int y);
protected:
	bool mbBound;
	bool mbVisible;
	bool mbUpdate;
	bool mbUseTouch;
	bool mbUseJoyBtn;
	bool mbUseJoyAxis;
	int miTag;
	unsigned int miColor;
	Node* mpParent;
	Render* mpRender;
	float mfAngle;
	Point mPosition;
	Point mWorldPos;
	Point mAnchor;
	Point mScale;
	Rect mFrame;
	ChildList mChildlist;
};
    
class NodeDispatcher : public Dispatcher {
public:
    static std::string makeKey(Node* target) {
        char buf[256] = {0};
        sprintf(buf, "node:update:%p", target);
        return buf;
    }
public:
	NodeDispatcher(Node* target) {
		this->target = target;
		this->setDispatchKey(NodeDispatcher::makeKey(target));
	}
	~NodeDispatcher() {
		this->target = NULL;
	}
public:
	virtual void update(float dt) {
		if (this->target) {
			this->target->update(dt);
		}
	}
private:
	Node* target;
};

class Texture;
class NodePopEffect : public Node {
public:
	typedef std::function<void(Node*)> FinishFunc;
	typedef enum {
		ANIMATE_NONE = 0,
		ANIMATE_IN   = 1,
		ANIMATE_OUT  = 2,
	} AnimateStyle;
public:
	NodePopEffect(Node* child);
	~NodePopEffect();
public:
	void setTexture(Texture* tex);
public:
	void setCompletedCallBack(FinishFunc func);
	bool playAnimation(AnimateStyle style, float duration);
	void stopAnimation();
	bool isPlaying() const {
		return this->mbPlaying;
	}
	bool getAnimateStyle() const {
		return this->mkAnimateStyle;
	}
public:
	Node* getTargetNode() const {
		return this->mpTarget;
	}
private:
	virtual void onKeyDown(APP_CTRL_TYPE key);
private:
	virtual void onUpdate(float dt);
	virtual void onRender();
private:
	virtual void onAttach();
	virtual void onDetach();
private:
	bool mbPlaying;
	AnimateStyle mkAnimateStyle;
	Texture* mpBG;
	Node* mpTarget;
	float mfScale;
	float mfDuration;
	float mfCurValue;
	Rect mOriginFrame;
	FinishFunc mCallBack;
};

} // lite2d

#endif // LITE2D_NODE_H
