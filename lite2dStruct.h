
#ifndef LITE2D_STRUCT_CPP
#define LITE2D_STRUCT_CPP

#include "stdio.h"

namespace lite2d {

typedef struct Point {
	float x;
	float y;
} Point;

typedef struct Size {
	float width;
	float height;
} Size;

typedef struct Rect {
	Point origin;
	Size size;
} Rect;

Point makePoint(float x, float y);
Size makeSize(float width, float height);
Rect makeRect(float x, float y, float width, float height);

} // lite2d

#endif // LITE2D_STRUCT_CPP