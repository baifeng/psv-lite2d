
#ifndef GAME_MENU_H
#define GAME_MENU_H

#include "lite2dNode.h"
#include "lite2dEvent.h"

namespace lite2d {
	class Button;
	class Sprite;
	class Label;
} // lite2d

class WidthHeightBomb {
public:
	WidthHeightBomb() {
		this->iWidth = 0;
		this->iHeight = 0;
		this->iBombCount = 0;
	}
	virtual ~WidthHeightBomb(){}
public:
	unsigned int iWidth;
	unsigned int iHeight;
	unsigned int iBombCount;
};

class NumberView : public lite2d::Node {
public:
	NumberView();
	~NumberView();
public:
	void setFrame(float x, float y, float width, float height);
	void setValue(int v);
private:
	lite2d::Sprite* mpSprite[3];
};

class OptionView;
class StateView : public lite2d::Node, public WidthHeightBomb {
public:
	typedef std::function<void()> CallBack0;
public:
	StateView();
	~StateView();
public:
	void setMineValue(int v);
	void setTimeValue(int v);
	void setMapChanged(CallBack0 f);
	void setPlayFunc(CallBack0 f);
public:
	void setInfoText(std::string const& text);
private:
	void showOptionView(lite2d::Node* sender);
	void startGame(lite2d::Node* sender);
	void updateMapInfo(OptionView* sender);
	void handleEvent(lite2d::Event const& args);
private:
	virtual void onAttach();
	virtual void onDetach();
private:
	NumberView* mpMineCountView;
	NumberView* mpTimeValueView;
	lite2d::Button* mpOptionBtn;
	lite2d::Button* mpStartBtn;
	lite2d::Label* mpInfoLabel;
	CallBack0 mOptionFunc;
	CallBack0 mPlayFunc;
};

class GameView;
class TileMapProxy;
class GameStateView : public lite2d::Node {
public:
	GameStateView();
	~GameStateView();
private:
	virtual void onAttach();
	virtual void onDetach();
private:
	virtual void onUpdate(float dt);
private:
	void startGame();
	void updateMapTips();
	void handleEvent(lite2d::Event const& args);
private:
	unsigned int miMapWidth;
	unsigned int miMapHeight;
	unsigned int miMineCount;
	StateView* mpStateView;
	GameView* mpGameView;
	TileMapProxy* mpTileMap;
	unsigned int miTimeValue;
	float mfTimeTick;
};

class OptionItemView : public lite2d::Node {
public:
	typedef std::function<void(lite2d::Node*)> ChangedFunc;
public:
	OptionItemView();
	~OptionItemView();
public:
	void setChangedFunc(ChangedFunc func);
public:
	void setMinMax(int min, int max);
	void setTitle(std::string const& text);
public:
	void setNumValue(int v);
	int getNumValue() const;
private:
	void onAdd(lite2d::Node* sender);
	void onSub(lite2d::Node* sender);
private:
	virtual void onUpdate(float dt);
private:
	int miValue;
	int miMin, miMax;
	lite2d::Label* mpTitle;
	NumberView* mpNumView;
	std::string mTitle;
	ChangedFunc mCallBack;
};

class OptionView : public lite2d::Node, public WidthHeightBomb {
public:
	typedef std::function<void(OptionView*)> OutputFunc;
public:
	OptionView();
	~OptionView();
public:
	void fadeIn();
	void fadeOut();
public:
	void setValue(int width, int height, int bomb);
	void setOutputFunc(OutputFunc func);
private:
	int getValue(int index) const;
	OptionItemView* getItemView(int index) const;
private:
	void onConfigChanged(lite2d::Node* sender);
	void onFadeOutFinish(lite2d::Node* sender);
private:
	virtual bool onTouchBegin(int x, int y);
	virtual void onTouchEnded(int x, int y);
private:
	lite2d::NodePopEffect* mpEffectBox;
	OutputFunc mOutputFunc;
};

#endif // GAME_MENU_H
