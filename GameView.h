
#ifndef GAME_VIEW_H
#define GAME_VIEW_H

#include "lite2dNode.h"
#include "lite2dEvent.h"

class TileMapProxy;
class GameView : public lite2d::Node
{
public:
	GameView();
	~GameView();
private:
	virtual void onAttach();
	virtual void onDetach();
private:
	virtual void onKeyDown(lite2d::APP_CTRL_TYPE key);
	virtual void onKeyUp(lite2d::APP_CTRL_TYPE key);
private:
	virtual bool onTouchBegin(int x, int y);
	virtual void onTouchMoved(int x, int y);
	virtual void onTouchEnded(int x, int y);
private:
	virtual void onRender();
private:
	void initGame();
	void setCurTileNull();
private:
	void handleEvent(lite2d::Event const& args);
private:
	void showGrid();
private:
	bool mbCanMoveX;
	bool mbCanMoveY;
	bool mbFlagMode;
	TileMapProxy* mpTileMap;
	int miOffsetX, miOffsetY;
	lite2d::Point mLastPt;
	lite2d::Point mCurrTile;
};

#endif // GAME_VIEW_H
