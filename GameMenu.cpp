
#include "GameMenu.h"
#include "GameView.h"
#include "LandMine.h"
#include "lite2d.h"
#include "math.h"
#include "string.h"

NumberView::NumberView() {
	for (int i = 0; i < 3; ++i) {
		this->mpSprite[i] = new lite2d::Sprite();
		this->addChild(this->mpSprite[i]);
	}
}

NumberView::~NumberView() {

}

void NumberView::setFrame(float x, float y, float width, float height) {
	Node::setFrame(x, y, width, height);
	float w0 = width / 3.0;
	for (int i = 0; i < 3; ++i) {
		this->mpSprite[i]->setFrame(i * w0, 0, w0, height);
	}
}

void NumberView::setValue(int v) {
	if (fabs(v) > 999) {
		return;
	}
	char buf[64] = {0};
	sprintf(buf, "%03d", v);
	if (strlen(buf) > 3) {
		return;
	}
	char fileName[64] = {0};
	for (int i = 0; i < (int)strlen(buf); ++i) {
		if (buf[i] == '-') {
			this->mpSprite[i]->loadTexture("number_dash.png");
			continue;
		}
		sprintf(fileName, "number_%c.png", buf[i]);
		this->mpSprite[i]->loadTexture(fileName);
	}
}



StateView::StateView() {
	lite2d::Button* btn = new lite2d::Button();
    btn->setTitle("", lite2d::Button::STATE_NORMAL);
    btn->setFrame(0, 0, 72, 72);
    btn->setPosition(100, 480);
    btn->setUpFunc(STDBIND1(&StateView::startGame, this));
    this->addChild(btn);
    this->mpStartBtn = btn;

    btn = new lite2d::Button();
    btn->setTitle("", lite2d::Button::STATE_NORMAL);
    btn->setFrame(0, 0, 72, 72);
    btn->setPosition(100, 390);
    btn->setUpFunc(STDBIND1(&StateView::showOptionView, this));
    btn->setImage(lite2d::getTexture("option.png"), lite2d::Button::STATE_NORMAL);
    btn->setImage(lite2d::getTexture("option_down.png"), lite2d::Button::STATE_HIGHLIGHT);
    this->addChild(btn);
    this->mpOptionBtn = btn;

    this->mpMineCountView = new NumberView();
    this->mpMineCountView->setFrame(0, 0, 102, 60);
    this->mpMineCountView->setPosition(100, 86);
    this->mpMineCountView->showBounds(true);
    this->addChild(this->mpMineCountView);

    this->mpTimeValueView = new NumberView();
    this->mpTimeValueView->setFrame(0, 0, 102, 60);
    this->mpTimeValueView->setPosition(100, 186);
    this->mpTimeValueView->showBounds(true);
    this->addChild(this->mpTimeValueView);

    this->mpInfoLabel = new lite2d::Label();
    this->mpInfoLabel->setTextColor(RGBA8(255, 255, 255, 255));
    this->mpInfoLabel->setPosition(100, 290);
    this->mpInfoLabel->setText("10x9x20");
    this->addChild(this->mpInfoLabel);

	this->mOptionFunc = NULL;
    this->mPlayFunc = NULL;
}

StateView::~StateView() {

}

void StateView::setMineValue(int v) {
	this->mpMineCountView->setValue(v);
}

void StateView::setTimeValue(int v) {
	this->mpTimeValueView->setValue(v >= 999 ? 999 : v);
}

void StateView::setMapChanged(CallBack0 f) {
	this->mOptionFunc = f;
}

void StateView::setPlayFunc(CallBack0 f) {
	this->mPlayFunc = f;
}

void StateView::setInfoText(std::string const& text) {
	this->mpInfoLabel->setText(text);
}

void StateView::showOptionView(lite2d::Node* sender) {
	OptionView* optionView = new OptionView();
	optionView->setValue(this->iWidth, this->iHeight, this->iBombCount);
	optionView->setOutputFunc(STDBIND1(&StateView::updateMapInfo, this));
	optionView->fadeIn();
	lite2d::getRootWindow().addChild(optionView);
}

void StateView::startGame(lite2d::Node* sender) {
	if (this->mPlayFunc) {
		this->mPlayFunc();
	}
}

void StateView::updateMapInfo(OptionView* sender) {
	this->iWidth = sender->iWidth;
	this->iHeight = sender->iHeight;
	this->iBombCount = sender->iBombCount;
	if (this->iBombCount > this->iWidth * this->iHeight) {
		this->iBombCount = this->iWidth * this->iHeight;
	}
	if (this->mOptionFunc) {
		this->mOptionFunc();
	}
}

void StateView::handleEvent(lite2d::Event const& args) {

	if (args.msgType() == TileMapProxy::GameStartEvent) {

		lite2d::Button* btn = this->mpStartBtn;
    	btn->setImage(lite2d::getTexture("smile1.png"), lite2d::Button::STATE_NORMAL);
    	btn->setImage(lite2d::getTexture("smile1_down.png"), lite2d::Button::STATE_HIGHLIGHT);

	}else if (args.msgType() == TileMapProxy::GameOverEvent) {

		lite2d::Button* btn = this->mpStartBtn;
    	btn->setImage(lite2d::getTexture("smile2.png"), lite2d::Button::STATE_NORMAL);
    	btn->setImage(lite2d::getTexture("smile2_down.png"), lite2d::Button::STATE_HIGHLIGHT);

	}else if (args.msgType() == TileMapProxy::GameClearEvent) {

		lite2d::Button* btn = this->mpStartBtn;
    	btn->setImage(lite2d::getTexture("smile3.png"), lite2d::Button::STATE_NORMAL);
    	btn->setImage(lite2d::getTexture("smile3_down.png"), lite2d::Button::STATE_HIGHLIGHT);

	}else if (args.msgType() == TileMapProxy::MineUpdateEvent) {

		lite2d::IntEvent const& _args = (lite2d::IntEvent const&)args;
		this->setMineValue(_args.msgValue());
	}
}

void StateView::onAttach() {
	lite2d::registerHandler(TileMapProxy::GameStartEvent,
		new lite2d::Handler<StateView>(this, &StateView::handleEvent));
	lite2d::registerHandler(TileMapProxy::GameOverEvent,
		new lite2d::Handler<StateView>(this, &StateView::handleEvent));
	lite2d::registerHandler(TileMapProxy::GameClearEvent,
		new lite2d::Handler<StateView>(this, &StateView::handleEvent));
	lite2d::registerHandler(TileMapProxy::MineUpdateEvent,
		new lite2d::Handler<StateView>(this, &StateView::handleEvent));
}

void StateView::onDetach() {
	lite2d::removeHandler(TileMapProxy::GameStartEvent, this);
	lite2d::removeHandler(TileMapProxy::GameOverEvent, this);
	lite2d::removeHandler(TileMapProxy::GameClearEvent, this);
	lite2d::removeHandler(TileMapProxy::MineUpdateEvent, this);
}


GameStateView::GameStateView() {

	this->miMapWidth = 11;
	this->miMapHeight = 8;
	this->miMineCount = this->miMapWidth * this->miMapHeight * 0.1;

	this->miTimeValue = 0;
	this->mfTimeTick = 0;

	this->mpTileMap = &(TileMapProxy&)lite2d::getProxy(TileMapProxy::ProxyName);

	this->mpGameView = new GameView();
	this->mpGameView->setFrame(200, 0, 960-200, 544);
	this->addChild(this->mpGameView);

	this->mpStateView = new StateView();
	this->mpStateView->setFrame(0, 0, 200, 544);
	this->mpStateView->setNodeColor(RGBA8(216, 216, 216, 255));
	this->mpStateView->setMapChanged(STDBIND(&GameStateView::updateMapTips, this));
	this->mpStateView->setPlayFunc(STDBIND(&GameStateView::startGame, this));
	this->mpStateView->iWidth = this->miMapWidth;
	this->mpStateView->iHeight = this->miMapHeight;
	this->mpStateView->iBombCount = this->miMineCount;
	this->addChild(this->mpStateView);

	this->mpStateView->setTimeValue(0);
	this->mpStateView->setMineValue(this->miMineCount);

	this->updateMapTips();
}

GameStateView::~GameStateView() {
	this->mpGameView = NULL;
	this->mpStateView = NULL;
	this->mpTileMap = NULL;
}

void GameStateView::onAttach() {
	lite2d::registerHandler(TileMapProxy::GameStartEvent,
		new lite2d::Handler<GameStateView>(this, &GameStateView::handleEvent));
	lite2d::registerHandler(TileMapProxy::GameOverEvent,
		new lite2d::Handler<GameStateView>(this, &GameStateView::handleEvent));
	lite2d::registerHandler(TileMapProxy::GameClearEvent,
		new lite2d::Handler<GameStateView>(this, &GameStateView::handleEvent));

	this->startGame();
}

void GameStateView::onDetach() {
	lite2d::removeHandler(TileMapProxy::GameStartEvent, this);
	lite2d::removeHandler(TileMapProxy::GameOverEvent, this);
	lite2d::removeHandler(TileMapProxy::GameClearEvent, this);
}

void GameStateView::onUpdate(float dt) {
	TileMapProxy& tileMap = *this->mpTileMap;
	if (tileMap.isGameOver() || tileMap.isGameClear()) {
		return;
	}
	this->mfTimeTick += dt;
	if (this->mfTimeTick >= 1.0f) {
		this->mfTimeTick -= 1.0f;
		this->miTimeValue ++;
		this->mpStateView->setTimeValue( this->miTimeValue );
	}
}

void GameStateView::updateMapTips() {

	this->miMapWidth = this->mpStateView->iWidth;
	this->miMapHeight = this->mpStateView->iHeight;
	this->miMineCount = this->mpStateView->iBombCount;

	char buf[256] = {0};
	sprintf(buf, "%dx%dx%d", this->miMapWidth, this->miMapHeight, this->miMineCount);
	this->mpStateView->setInfoText(buf);

	//this->startGame();
}

void GameStateView::startGame() {
	TileMapProxy& tileMap = *this->mpTileMap;
	tileMap.start(this->miMapWidth, this->miMapHeight, this->miMineCount);
	this->mpStateView->setMineValue(this->miMineCount);
}

void GameStateView::handleEvent(lite2d::Event const& args) {

	if (args.msgType() == TileMapProxy::GameStartEvent) {

		this->miTimeValue = 0;
		this->mfTimeTick = 0;
		this->mpStateView->setTimeValue( 0 );
		this->registerUpdate();

	}else if (args.msgType() == TileMapProxy::GameOverEvent) {

		this->removeUpdate();

	}else if (args.msgType() == TileMapProxy::GameClearEvent) {

		this->removeUpdate();

	}
}


OptionItemView::OptionItemView() {
	this->setFrame(0, 0, 220, 80);
	//this->registerUpdate();

	this->mpTitle = new lite2d::Label();
	this->mpTitle->setTextColor(WHITE);
	this->mpTitle->setPosition(110, 14);
	this->addChild(this->mpTitle);

	this->mpNumView = new NumberView();
	this->mpNumView->setFrame(0, 0, 66, 40);
	this->mpNumView->setPosition(110, 56);
	this->addChild(this->mpNumView);

	lite2d::Button* left = new lite2d::Button();
	left->setTitleColor(WHITE, lite2d::Button::STATE_NORMAL);
	left->setTitleColor(GREEN, lite2d::Button::STATE_HIGHLIGHT);
	left->setTitle("-", lite2d::Button::STATE_NORMAL);
	left->setImage(lite2d::getTexture("tile_mask.png"), lite2d::Button::STATE_NORMAL);
	left->setImage(lite2d::getTexture("tile_down.png"), lite2d::Button::STATE_HIGHLIGHT);
	left->setUpFunc(STDBIND1(&OptionItemView::onSub, this));
	left->setFrame(0, 0, 44, 44);
	left->setPosition(38, 52);
	this->addChild(left);

	lite2d::Button* right = new lite2d::Button();
	right->setTitleColor(WHITE, lite2d::Button::STATE_NORMAL);
	right->setTitleColor(GREEN, lite2d::Button::STATE_HIGHLIGHT);
	right->setTitle("+", lite2d::Button::STATE_NORMAL);
	right->setImage(lite2d::getTexture("tile_mask.png"), lite2d::Button::STATE_NORMAL);
	right->setImage(lite2d::getTexture("tile_down.png"), lite2d::Button::STATE_HIGHLIGHT);
	right->setUpFunc(STDBIND1(&OptionItemView::onAdd, this));
	right->setFrame(0, 0, 44, 44);
	right->setPosition(220-38, 52);
	this->addChild(right);

	//this->mpTitle->setText("title");
	this->mpNumView->setValue(0);
	this->mCallBack = NULL;
}

OptionItemView::~OptionItemView() {

}

void OptionItemView::setChangedFunc(ChangedFunc func) {
	this->mCallBack = func;
}

void OptionItemView::setTitle(std::string const& text) {
	this->mpTitle->setText(text);
	this->mTitle = text;
}

void OptionItemView::setNumValue(int v) {
	this->miValue = v;
	this->mpNumView->setValue(this->miValue);
}

int OptionItemView::getNumValue() const {
	return this->miValue;
}

void OptionItemView::setMinMax(int min, int max) {
	this->miMin = min;
	this->miMax = max;
}

void OptionItemView::onAdd(lite2d::Node* sender) {
	this->miValue++;
	if (this->miValue >= this->miMax) {
		this->miValue = this->miMax;
		this->mpTitle->setTextColor(RED);
		this->mpTitle->setText(this->mTitle+"(MAX)");
	}else {
		lite2d::Button* btn = (lite2d::Button*)sender;
		if (btn->getTitleView()->getText() != this->mTitle) {
			this->mpTitle->setTextColor(WHITE);
			this->mpTitle->setText(this->mTitle);
		}
	}
	this->setNumValue(this->miValue);

	if (this->mCallBack) {
		this->mCallBack(this);
	}
}

void OptionItemView::onSub(lite2d::Node* sender) {
	this->miValue--;
	if (this->miValue <= this->miMin) {
		this->miValue = this->miMin;
		this->mpTitle->setTextColor(RED);
		this->mpTitle->setText(this->mTitle+"(MIN)");
	}else {
		lite2d::Button* btn = (lite2d::Button*)sender;
		if (btn->getTitleView()->getText() != this->mTitle) {
			this->mpTitle->setTextColor(WHITE);
			this->mpTitle->setText(this->mTitle);
		}
	}
	this->setNumValue(this->miValue);

	if (this->mCallBack) {
		this->mCallBack(this);
	}
}

void OptionItemView::onUpdate(float dt) {

}


OptionView::OptionView() {

	this->registerTouch();

	lite2d::Node* box = new lite2d::Node();
	box->setFrame(0, 0, 220, 270);
	box->registerTouch();

	std::string title[3] = {
		"WIDTH", "HEIGHT", "BOMB"
	};

	for (int i = 0; i < 3; ++i) {
		OptionItemView* itemView = new OptionItemView();
		itemView->setMinMax(1, 25);
		itemView->setTitle(title[i]);
		itemView->setTag(i+1);
		itemView->setChangedFunc(STDBIND1(&OptionView::onConfigChanged, this));
		itemView->setFrame(0, 10+85*i, 220, 80);
		box->addChild(itemView);
	}

	this->mpEffectBox = new lite2d::NodePopEffect(box);
	this->mpEffectBox->setNodeColor(RGBA8(120, 120, 120, 220));
	this->mpEffectBox->setFrame(0, 0, 220, 270);
	this->mpEffectBox->setAnchor(lite2d::makePoint(0.1f, 0.9f));
	this->mpEffectBox->setPosition(100, 390);
	//this->mpEffectBox->registerTouch();
	this->addChild(this->mpEffectBox);

	this->mOutputFunc = NULL;
}

OptionView::~OptionView() {
	
}

void OptionView::fadeIn() {
	this->mpEffectBox->playAnimation(lite2d::NodePopEffect::ANIMATE_IN, 0.2f);
}

void OptionView::fadeOut() {
	if (this->mpEffectBox->isPlaying()) {
		return;
	}
	this->mpEffectBox->setCompletedCallBack(STDBIND1(&OptionView::onFadeOutFinish, this));
	this->mpEffectBox->playAnimation(lite2d::NodePopEffect::ANIMATE_OUT, 0.16f);
}

int OptionView::getValue(int index) const {
	OptionItemView* view = this->getItemView(index);
	if (view != NULL) {
		return view->getNumValue();
	}
	return 1;
}

OptionItemView* OptionView::getItemView(int index) const {
	lite2d::Node* node = this->mpEffectBox->getTargetNode();
	OptionItemView* view = (OptionItemView*)node->getChildWithTag(index+1);
	return view;
}

void OptionView::setValue(int width, int height, int bomb) {
	OptionItemView* view = this->getItemView(0);
	if (view) {
		view->setNumValue(width);
	}
	view = this->getItemView(1);
	if (view) {
		view->setNumValue(height);
	}
	view = this->getItemView(2);
	if (view) {
		view->setNumValue(bomb);
	}
}

void OptionView::setOutputFunc(OutputFunc func) {
	this->mOutputFunc = func;
}

void OptionView::onConfigChanged(lite2d::Node* sender) {
	OptionItemView* width = this->getItemView(0);
	OptionItemView* height = this->getItemView(1);
	OptionItemView* bomb = this->getItemView(2);
	if (width && height && bomb) {
		bomb->setMinMax(1, width->getNumValue() * height->getNumValue());
	}
}

void OptionView::onFadeOutFinish(lite2d::Node* sender) {
	this->iWidth = this->getValue(0);
	this->iHeight = this->getValue(1);
	this->iBombCount = this->getValue(2);
	if (this->mOutputFunc) {
		this->mOutputFunc(this);
	}
	this->removeFromParent();
	delete this;
}

bool OptionView::onTouchBegin(int x, int y) {
	return true;
}

void OptionView::onTouchEnded(int x, int y) {
	this->fadeOut();
}
