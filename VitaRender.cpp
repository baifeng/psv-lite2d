
#include "VitaRender.h"
#include "lite2dTexture.h"
#include "lite2dFont.h"
#include "vita2d.h"
#include <stdarg.h>

namespace lite2d {

static VitaRender* render = NULL;

Render* sharedRender() {
	if (render==NULL) {
		render = new VitaRender();
	}
	return render;
}

void destoryRender() {
	if (render!=NULL) {
		delete render;
		render = NULL;
	}
}

} // lite2d

VitaRender::VitaRender() {
	this->mClipStack.reserve(256);
}

void VitaRender::beginClip(int x, int y, int w, int h) {

}

void VitaRender::endClip() {

}

void VitaRender::drawpixel(float x, float y, unsigned int color) {
	vita2d_draw_pixel(x, y, color);
}

void VitaRender::drawline(float x0, float y0, float x1, float y1, unsigned int color) {
	vita2d_draw_line(x0, y0, x1, y1, color);
}

void VitaRender::drawrect(float x, float y, float w, float h, unsigned int color) {
	vita2d_draw_rectangle(x, y, w, h, color);
}

void VitaRender::drawcircle(float x, float y, float radius, unsigned int color) {
	vita2d_draw_fill_circle(x, y, radius, color);
}

void VitaRender::draw(lite2d::Texture* texture, float x, float y) {
	vita2d_draw_texture((vita2d_texture*)texture->getTexture(), x, y);
}

void VitaRender::draw(lite2d::Texture* texture, float x, float y, unsigned int w, unsigned h) {
	vita2d_draw_texture_scale((vita2d_texture*)texture->getTexture(), x, y,
		double(w) / texture->getWidth(), double(h) / texture->getHeight());
}

void VitaRender::drawtext(lite2d::Font* font, float x, float y, char const* text, ...) {
	char buf[1024];
	va_list argptr;
	va_start(argptr, text);
	vsnprintf(buf, sizeof(buf), text, argptr);
	va_end(argptr);
	vita2d_pgf_draw_text((vita2d_pgf*)font->getFont(), x, y+font->getFontScale()*DEFAULT_FONT_SIZE,
		font->getFontColor(), font->getFontScale(), buf);
}
