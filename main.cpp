#define MAJOR_VERSION 1
#define MINOR_VERSION 2

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <psp2/ctrl.h>
#include <psp2/kernel/processmgr.h>
#include <psp2/touch.h>

#include <vita2d.h>

#include "Field.h"
#include "lite2d.h"
#include "MyApp.h"

#define GAME_STATE_MENU 				0x00
#define GAME_STATE_SUBMENU_DIFFICULTY 	0x01
#define GAME_STATE_CREATE_BASE 			0x02
#define GAME_STATE_BATTLE 				0x03
#define GAME_STATE_FINISH				0x04

// Controls -----------------------------------------------------------------------------------
#define lerp(value, from_max, to_max) ((((value * 10) * (to_max * 10)) / (from_max * 10)) / 10)
SceCtrlData  oldPad, newPad;
SceTouchData touch;
int fxTouch;
int fyTouch;
bool ftouched = false;
int bxTouch;
int byTouch;
bool btouched = false;

unsigned char cursorX, cursorY; // Create base object

// Graphics objects ---------------------------------------------------------------------------	
vita2d_texture *background;
vita2d_texture *fieldimg;
vita2d_texture *items;
vita2d_pgf *pgf;

extern unsigned char _binary_assets_gui_background_png_start;
extern unsigned char _binary_assets_gui_field_png_start;
extern unsigned char _binary_assets_gui_items_png_start;

#define BLACK   RGBA8(  0,   0,   0, 255)
#define WHITE   RGBA8(255, 255, 255, 255)
#define GREEN   RGBA8(  0, 255,   0, 255)
#define RED     RGBA8(255,   0,   0, 255)
#define BLUE    RGBA8(  0,   0, 255, 255)
#define PURPLE  RGBA8(255,   0, 255, 255)

// Game objects -------------------------------------------------------------------------------
bool done = false;
unsigned char gameState = GAME_STATE_MENU;
Field playerOneField, playerTwoField;
float dt = 0.0f;
bool playerOneTurn = false;
bool playerOneWin = false;
unsigned char gameStatistic[2];

// Functions ----------------------------------------------------------------------------------
void initGame()
{
	vita2d_init();
	vita2d_set_clear_color(RGBA8(255, 255, 255, 255));
	
	background = vita2d_load_PNG_buffer(&_binary_assets_gui_background_png_start);
	fieldimg = vita2d_load_PNG_buffer(&_binary_assets_gui_field_png_start);
	items = vita2d_load_PNG_buffer(&_binary_assets_gui_items_png_start);
	pgf = vita2d_load_default_pgf();

	memset(&oldPad, 0, sizeof(oldPad));
	memset(&newPad, 0, sizeof(newPad));

    sceCtrlSetSamplingMode(SCE_CTRL_MODE_ANALOG_WIDE);
    sceTouchSetSamplingState(SCE_TOUCH_PORT_FRONT, SCE_TOUCH_SAMPLING_STATE_START);
    sceTouchSetSamplingState(SCE_TOUCH_PORT_BACK, SCE_TOUCH_SAMPLING_STATE_START);
}

void termGame()
{
	vita2d_fini();
	vita2d_free_texture(background);
	vita2d_free_texture(fieldimg);
	vita2d_free_texture(items);
	vita2d_free_pgf(pgf);	
}

bool isJoyButtonPressed(unsigned int button)
{
	if (!(oldPad.buttons & button))
	{
		if (newPad.buttons & button)
		{
			return true;
		}
	}
	return false;	
}

bool isJoyButtonHold(unsigned int button)
{
	if (newPad.buttons & button)
	{
		return true;
	}
	return false;	
}
		
#define JOY_AXIS_LEFT_X	 0x00
#define JOY_AXIS_LEFT_Y	 0x01
#define JOY_AXIS_RIGHT_X 0x02
#define JOY_AXIS_RIGHT_Y 0x03

float getJoyAxis(unsigned char type)
{
	switch (type)
	{
	case 0:
		return (float)newPad.lx - 127.5f;
		break;

	case 1:
		return (float)newPad.ly - 127.5f;
		break;

	case 2:
		return (float)newPad.rx - 127.5f;
		break;

	case 3:
		return (float)newPad.ry - 127.5f;
		break;

	default:
		return 0.0f;
		break;
	}
	
	return 0.0f;
}

int getFrontTouchX()
{
	return fxTouch;
}

int getFrontTouchY()
{
	return fyTouch;
}

int getBackTouchX()
{
	return bxTouch;
}

int getBackTouchY()
{
	return byTouch;
}

// draw field -----------------------------------------------------------------------------------------------
unsigned int fireAnimation = 0;
float animationTime = 0.0f;

void drawField(Field *field, unsigned short x, unsigned short y, bool own)
{
	vita2d_draw_texture(fieldimg, (float)x - 30.0f, (float)y - 30.0f);

	animationTime += dt;
	if (animationTime > 0.1f)
	{
		animationTime = 0.0f;
		fireAnimation++;
		if (fireAnimation > 2) fireAnimation = 0;
	}
	
	for (int i = 0; i < FIELD_SIZE; i++)
	{
		for (int j = 0; j < FIELD_SIZE; j++)
		{
			unsigned char cell = field->getCellValue(i, j);
			unsigned char tmpCell;

			switch (own)
			{
			case true:			
				if (cell == CELL_MISS) vita2d_draw_texture_part(items, i * 30 + x, j * 30 + y, 60, 30, 30, 30);
				else if (cell == CELL_BOOM) vita2d_draw_texture_part(items, i * 30 + x, j * 30 + y, fireAnimation * 30, 60, 30, 30);
				else if (cell != 0) 
				{
					//vita2d_draw_rectangle(i * 30 + x, j * 30 + y, 30, 30, RGBA8(127, 127, 127, 255));
					//	2
					//0	  1
					//	3
					bool nearCells[4];
					for (int t = 0; t < 4; t++) nearCells[t] = false;
					unsigned char tileX, tileY;

					// Left			
					if (i > 0)
					{
						tmpCell = field->getCellValue(i - 1, j);
						if ((tmpCell != 0) && (tmpCell != CELL_MISS)) nearCells[0] = true;
					}

					// Right			
					if (i < 9)
					{
						tmpCell = field->getCellValue(i + 1, j);
						if ((tmpCell != 0) && (tmpCell != CELL_MISS)) nearCells[1] = true;
					}

					// Top
					if (j > 0)
					{
						tmpCell = field->getCellValue(i, j - 1);
						if ((tmpCell != 0) && (tmpCell != CELL_MISS)) nearCells[2] = true;
					}		

					// Bottom		
					if (j < 9)
					{
						tmpCell = field->getCellValue(i, j + 1);
						if ((tmpCell != 0) && (tmpCell != CELL_MISS)) nearCells[3] = true;
					}	

					// Find tile
					if (!nearCells[0] && !nearCells[1] && !nearCells[2] && !nearCells[3])
					{
						tileX = i % 2; //random XD
						tileY = 1;
					}
					else if (!nearCells[0] && !nearCells[2] && !nearCells[3])
					{
						tileX = 0;
						tileY = 0;
					}
					else if (!nearCells[0] && !nearCells[2] && !nearCells[1])
					{
						tileX = 3;
						tileY = 0;
					}
					else if (!nearCells[1] && !nearCells[2] && !nearCells[3])
					{
						tileX = 2;
						tileY = 0;
					}
					else if (!nearCells[1] && !nearCells[0] && !nearCells[3])
					{
						tileX = 3;
						tileY = 2;
					}
					else if (!nearCells[0] && !nearCells[1])
					{
						tileX = 3;
						tileY = 1;
					}
					else if (!nearCells[2] && !nearCells[3])
					{
						tileX = 1;
						tileY = 0;
					}
					vita2d_draw_texture_part(items, i * 30 + x, j * 30 + y, tileX * 30, tileY * 30, 30, 30);
				}
				break;

			case false:
				if (cell == CELL_MISS) vita2d_draw_texture_part(items, i * 30 + x, j * 30 + y, 60, 30, 30, 30);
				else if (cell == CELL_BOOM) vita2d_draw_texture_part(items, i * 30 + x, j * 30 + y, fireAnimation * 30, 60, 30, 30);
				break;

			default:
				break;
			};

			
		}
	}
}

// Move cursor -----------------------------------------------------------------------------
void moveCursor()
{
	if (isJoyButtonPressed(SCE_CTRL_UP))
	{
		(cursorY == 0) ? cursorY = 9 : cursorY--;
	}

	if (isJoyButtonPressed(SCE_CTRL_DOWN))
	{
		(cursorY >= 9) ? cursorY = 0 : cursorY++;
	}

	if (isJoyButtonPressed(SCE_CTRL_LEFT))
	{
		(cursorX == 0) ? cursorX = 9 : cursorX--;
	}

	if (isJoyButtonPressed(SCE_CTRL_RIGHT))
	{
		(cursorX >= 9) ? cursorX = 0 : cursorX++;
	}

	if (ftouched)
	{
		if (gameState == GAME_STATE_CREATE_BASE)
		{
			if ((getFrontTouchX() > 90) && (getFrontTouchY() > 120) && (getFrontTouchX() < 390) && (getFrontTouchY() < 420))
			{
				cursorX = (getFrontTouchX() - 90) / 30;
				cursorY = (getFrontTouchY() - 120) / 30;
			}
		}
		else
		{
			if ((getFrontTouchX() > 90 + 480) && (getFrontTouchY() > 120) && (getFrontTouchX() < 390 + 480) && (getFrontTouchY() < 420))
			{
				cursorX = (getFrontTouchX() - (90 + 480)) / 30;
				cursorY = (getFrontTouchY() - 120) / 30;
			}		
		}
	}

	if (getJoyAxis(JOY_AXIS_LEFT_X) > 60.0f)
	{
		(cursorX >= 9) ? cursorX = 0 : cursorX++;
	}

	if (getJoyAxis(JOY_AXIS_LEFT_X) < -60.0f)
	{
		(cursorX == 0) ? cursorX = 9 : cursorX--;
	}

	if (getJoyAxis(JOY_AXIS_LEFT_Y) > 60.0f)
	{
		(cursorY >= 9) ? cursorY = 0 : cursorY++;
	}

	if (getJoyAxis(JOY_AXIS_LEFT_Y) < -60.0f)
	{
		(cursorY == 0) ? cursorY = 9 : cursorY--;
	}
}

// Menu ------------------------------------------------------------------------------------

unsigned char menuSelectedItem = 0;
char *menuItems[]= {"New game", "Settings", "About", "Exit"};

bool resetCreateBase = false;
bool resetBattle = false;

void menu()
{
	// Events
	if (isJoyButtonPressed(SCE_CTRL_UP))
	{
		(menuSelectedItem == 0) ? menuSelectedItem = 3 : menuSelectedItem--;
	}

	if (isJoyButtonPressed(SCE_CTRL_DOWN))
	{
		(menuSelectedItem == 3) ? menuSelectedItem = 0 : menuSelectedItem++;
	}

	if (isJoyButtonPressed(SCE_CTRL_CROSS))
	{
		switch (menuSelectedItem)
		{
		case 0: // New game
			playerOneField.fill(0);
			resetCreateBase = true;
			gameState = GAME_STATE_CREATE_BASE;
			break;

		case 1: // Settings
			break;

		case 2: // About
			break;

		case 3: // Exit
			done = true;
			break;

		default:
			break;
		}
	}	

	// Draw
	for (int i = 0; i < 4; i++)
	{
		vita2d_pgf_draw_text(pgf, 30, 240 + i * 20, (menuSelectedItem == i) ? BLACK : GREEN, 1.0f, menuItems[i]);
	}

	vita2d_pgf_draw_textf(pgf, 30, 532, BLACK, 1.0f, "version v%d.%d", MAJOR_VERSION, MINOR_VERSION);
	vita2d_pgf_draw_text(pgf, 820, 532, BLACK, 1.0f, "by DesiRED");
}

// Create base -----------------------------------------------------------------------------

#define BASE_OFFSET_X 90
#define BASE_OFFSET_Y 120

unsigned char createBaseState;
unsigned char shipsCount;
ShipDirection shipDirection = SHIP_DIR_VERTICAL;

void createBase()
{
	//Reset
	if (resetCreateBase)
	{
		resetCreateBase = false;
		createBaseState = 4;
		shipsCount = 0;
	}

	// Events
	moveCursor();

	// Rotate ship
	if (isJoyButtonPressed(SCE_CTRL_CIRCLE))
	{
		switch (shipDirection)
		{
		case SHIP_DIR_VERTICAL:
			shipDirection = SHIP_DIR_HORIZONTAL;
			break;

		case SHIP_DIR_HORIZONTAL:
			shipDirection = SHIP_DIR_VERTICAL;
			break;

		default:
			break;
		};
	}

	// Clear base
	if (isJoyButtonPressed(SCE_CTRL_SQUARE))
	{
		playerOneField.fill(0);
		createBaseState = 4;
		shipsCount = 0;
	}

	// Generate random base
	if (isJoyButtonPressed(SCE_CTRL_TRIANGLE))
	{
		playerOneField.generate(4);
		createBaseState = 0;
	}

	// Place ship
	if (isJoyButtonPressed(SCE_CTRL_CROSS))
	{
		if (createBaseState > 0)
		{
			if (playerOneField.isTrueShipPlace(cursorX, cursorY, createBaseState, shipDirection))
			{
				playerOneField.placeShip(cursorX, cursorY, createBaseState, shipDirection);
				shipsCount++;

				if (shipsCount == 5 - createBaseState)
				{
					shipsCount = 0;
					createBaseState--;
				}
			}
		}
	}	

	// Back to menu
	if (isJoyButtonPressed(SCE_CTRL_SELECT))
	{
		gameState = GAME_STATE_MENU;
	}

	// Start game
	if (isJoyButtonPressed(SCE_CTRL_START))
	{
		if (createBaseState == 0)
		{
			playerTwoField.generate(4);
			cursorX = 0;
			cursorY = 0;
			gameStatistic[0] = 0;
			gameStatistic[1] = 0;
			playerOneTurn = (rand() % 2 == 0);
			resetBattle = true;
			gameState = GAME_STATE_BATTLE;
		}
	}

	// Draw
	vita2d_draw_texture(background, 0, 0);
	vita2d_pgf_draw_text(pgf, 30, 20, BLACK, 1.0f, "Setup base");
	drawField(&playerOneField, BASE_OFFSET_X, BASE_OFFSET_Y, true);

	// Draw preview
	if (createBaseState > 0)
	{
		switch (shipDirection)
		{
		case SHIP_DIR_VERTICAL:
			for (int i = 0; i < createBaseState; i++)
			{
				vita2d_draw_rectangle(cursorX * 30 + BASE_OFFSET_X, (cursorY + i) * 30 + BASE_OFFSET_Y, 30, 30, RGBA8(255, 0, 255, 127));
			}
			break;

		case SHIP_DIR_HORIZONTAL:
			for (int i = 0; i < createBaseState; i++)
			{
				vita2d_draw_rectangle((cursorX + i) * 30 + BASE_OFFSET_X, cursorY * 30 + BASE_OFFSET_Y, 30, 30, RGBA8(255, 0, 255, 127));
			}
			break;

		default:
			break;
		};
	}

	// Draw field cursor
	vita2d_draw_texture_part(items, BASE_OFFSET_X + cursorX * 30,  BASE_OFFSET_Y + cursorY * 30, 120, 0, 30, 30);

	// Draw controls help
	vita2d_pgf_draw_text(pgf, 510, 140, BLACK, 1.0f, "Controls:");
	vita2d_pgf_draw_text(pgf, 510, 170, BLACK, 1.0f, "[Triangle] Generate random base");
	vita2d_pgf_draw_text(pgf, 510, 200, BLACK, 1.0f, "[Square] Clear base");
	vita2d_pgf_draw_text(pgf, 510, 230, BLACK, 1.0f, "[Circle] Rotate ship");
	vita2d_pgf_draw_text(pgf, 510, 260, BLACK, 1.0f, "[Cross] Place ship");
	vita2d_pgf_draw_text(pgf, 510, 290, BLACK, 1.0f, "[Arrows] Move cursor");
	//vita2d_pgf_draw_text(pgf, 510, 290, BLACK, 1.0f, "[Select] Back to menu");
	//vita2d_pgf_draw_text(pgf, 510, 320, BLACK, 1.0f, "[Start] Start game");

	// Controls help
	vita2d_pgf_draw_text(pgf, 30, 532, BLACK, 1.0f, "[Select] Back to menu  [Start] Start game");
}

// Battle ----------------------------------------------------------------------------------

// AI
unsigned char aiCursorX, aiCursorY;
float aiTurnTime = 0.0f;
unsigned char aiLastHitX, aiLastHitY;
unsigned char aiAttackState;
unsigned char aiFindDirState;
unsigned char aiDirection;
unsigned char aiShipLength;

void aiCheckWin()
{
	if (playerOneField.unbrokenCellsCount() == 0)
	{
		playerOneWin = false;
		gameState = GAME_STATE_FINISH;
	}
}

void battle()
{
	// Reset
	if (resetBattle)
	{
		aiAttackState = 0;
		aiTurnTime = 0.0f;
		resetBattle = false;
	}

	// Events
	moveCursor();

	// Make step
	if (isJoyButtonPressed(SCE_CTRL_CROSS))
	{
		if (playerOneTurn)
		{
			if (!playerTwoField.isAttackedCell(cursorX, cursorY))
			{
				if (playerTwoField.attackCell(cursorX, cursorY))
				{
					gameStatistic[0]++;

					if (playerTwoField.unbrokenCellsCount() == 0)
					{
						playerOneWin = true;
						gameState = GAME_STATE_FINISH;
					}
				}
				else
				{
					gameStatistic[1]++;

					playerOneTurn = false;
				}
			}		
		}
	}	

	// Back to menu
	if (isJoyButtonPressed(SCE_CTRL_SELECT))
	{
		gameState = GAME_STATE_MENU;
	}

	//Computer turn 
	if (!playerOneTurn)
	{
		aiTurnTime += dt;
		if (aiTurnTime > 1.0f)
		{	
			switch (aiAttackState)
			{
			// Random attack
			case 0:
				do
				{
					resetCursor:
					aiCursorX = rand() % FIELD_SIZE;
					aiCursorY = rand() % FIELD_SIZE;	

					// Left side
					if (aiCursorX > 0)
					{
						if (aiCursorY > 0) 
						{
							if (playerOneField.isCrushCell(aiCursorX - 1, aiCursorY - 1)) goto resetCursor;
						}

						if (playerOneField.isCrushCell(aiCursorX - 1, aiCursorY)) goto resetCursor;

						if (aiCursorY < 9) 
						{
							if (playerOneField.isCrushCell(aiCursorX - 1, aiCursorY + 1)) goto resetCursor;
						}
					}

					// Middle
					if (aiCursorY > 0) 
					{
						if (playerOneField.isCrushCell(aiCursorX, aiCursorY - 1)) goto resetCursor;
					}

					if (aiCursorY < 9) 
					{
						if (playerOneField.isCrushCell(aiCursorX, aiCursorY + 1)) goto resetCursor;
					}

					// Right side
					if (aiCursorX < 9)
					{
						if (aiCursorY > 0) 
						{
							if (playerOneField.isCrushCell(aiCursorX + 1, aiCursorY - 1)) goto resetCursor;
						}

						if (playerOneField.isCrushCell(aiCursorX + 1, aiCursorY)) goto resetCursor;

						if (aiCursorY < 9) 
						{
							if (playerOneField.isCrushCell(aiCursorX + 1, aiCursorY + 1)) goto resetCursor;
						}
					}

				}
				while (playerOneField.isAttackedCell(aiCursorX, aiCursorY));

				if (!playerOneField.attackCell(aiCursorX, aiCursorY))
				{
					playerOneTurn = true;
				}
				else
				{
					aiLastHitX = aiCursorX;
					aiLastHitY = aiCursorY;
					aiAttackState++;
					aiFindDirState = 0;

					aiCheckWin();
				}
				break;

			// Find direction
			case 1:
				switch (aiFindDirState)
				{
				// Left
				case 0:
					if (aiLastHitX == 0) 
					{
						aiFindDirState++;
					}
					else
					{
						if (playerOneField.isAttackedCell(aiLastHitX - 1, aiLastHitY))
						{
							aiFindDirState++;
						}
						else
						{
							if (!playerOneField.attackCell(aiLastHitX - 1, aiLastHitY))
							{
								aiFindDirState++;
								playerOneTurn = true;
							}
							else
							{
								aiLastHitX--;
								aiAttackState++;
								aiDirection = 0;
								aiShipLength = 2;

								aiCheckWin();
							}
						}
					}
					break;

				// Top
				case 1:
					if (aiLastHitY == 0) 
					{
						aiFindDirState++;
					}
					else
					{
						if (playerOneField.isAttackedCell(aiLastHitX, aiLastHitY - 1))
						{
							aiFindDirState++;
						}
						else
						{
							if (!playerOneField.attackCell(aiLastHitX, aiLastHitY - 1))
							{
								aiFindDirState++;
								playerOneTurn = true;
							}
							else
							{
								aiLastHitY--;
								aiAttackState++;
								aiDirection = 1;
								aiShipLength = 2;

								aiCheckWin();
							}
						}
					}
					break;

				// Right
				case 2:
					if (aiLastHitX == 9) 
					{
						aiFindDirState++;
					}
					else
					{
						if (playerOneField.isAttackedCell(aiLastHitX + 1, aiLastHitY))
						{
							aiFindDirState++;
						}
						else
						{
							if (!playerOneField.attackCell(aiLastHitX + 1, aiLastHitY))
							{
								aiFindDirState++;
								playerOneTurn = true;
							}
							else
							{
								aiLastHitX++;
								aiAttackState++;
								aiDirection = 2;
								aiShipLength = 2;

								aiCheckWin();
							}
						}
					}
					break;

				// Bottom
				case 3:
					if (aiLastHitY == 9) 
					{
						aiFindDirState++;
					}
					else
					{
						if (playerOneField.isAttackedCell(aiLastHitX, aiLastHitY + 1))
						{
							aiFindDirState++;
						}
						else
						{
							if (!playerOneField.attackCell(aiLastHitX, aiLastHitY + 1))
							{
								aiFindDirState++;
								playerOneTurn = true;
							}
							else
							{
								aiLastHitY++;
								aiAttackState++;
								aiDirection = 3;
								aiShipLength = 2;

								aiCheckWin();
							}
						}
					}
					break;

				// If not find then this ship have only one cell
				default:
					aiAttackState = 0;
					break;
				}
				break;

				// Destroy ship
				case 2:
					switch (aiDirection)
					{
					// Left
					case 0:
						if ((int)aiLastHitX - (int)1 < (int)0)
						{
							aiDirection = 2;
							aiLastHitX = aiCursorX;
						}
						else
						{
							if (playerOneField.isAttackedCell(aiLastHitX - 1, aiLastHitY))
							{
								aiDirection = 2;
								aiLastHitX = aiCursorX;
							}
							else
							{
								if (!playerOneField.attackCell(aiLastHitX - 1, aiLastHitY))
								{
									aiDirection = 2;
									aiLastHitX = aiCursorX;
									playerOneTurn = true;
								}
								else
								{
									aiLastHitX--;
									aiShipLength++;

									aiCheckWin();
								}
							}
						}
						break;

					// Top
					case 1: 
						if ((int)aiLastHitY - (int)1 < (int)0)
						{
							aiDirection = 3;
							aiLastHitY = aiCursorY;
						}
						else
						{
							if (playerOneField.isAttackedCell(aiLastHitX, aiLastHitY - 1))
							{
								aiDirection = 3;
								aiLastHitY = aiCursorY;
							}
							else
							{
								if (!playerOneField.attackCell(aiLastHitX, aiLastHitY - 1))
								{
									aiDirection = 3;
									aiLastHitY = aiCursorY;
									playerOneTurn = true;
								}
								else
								{
									aiLastHitY--;
									aiShipLength++;

									aiCheckWin();
								}
							}
						}
						break;

					// Right
					case 2:
						if (aiLastHitX  == 9)
						{
							aiAttackState = 0;
						}
						else
						{
							if (playerOneField.isAttackedCell(aiLastHitX + 1, aiLastHitY))
							{
								aiAttackState = 0;
							}
							else
							{
								if (!playerOneField.attackCell(aiLastHitX + 1, aiLastHitY))
								{
									aiAttackState = 0;
									playerOneTurn = true;
								}
								else
								{
									aiLastHitX++;

									aiCheckWin();
								}
							}
						}
						break;

					// Bottom
					case 3:
						if (aiLastHitY == 9)
						{
							aiAttackState = 0;
						}
						else
						{
							if (playerOneField.isAttackedCell(aiLastHitX, aiLastHitY + 1))
							{
								aiAttackState = 0;
							}
							else
							{
								if (!playerOneField.attackCell(aiLastHitX, aiLastHitY + 1))
								{
									aiAttackState = 0;
									playerOneTurn = true;
								}
								else
								{
									aiLastHitY++;

									aiCheckWin();
								}
							}
						}
						break;

					default:
						aiAttackState = 0;
						break;
					};
					break;

			default:
				aiAttackState = 0;
				break;
			};	

			aiTurnTime = 0.0f;
		}
	}	

	// Draw
	vita2d_draw_texture(background, 0, 0);
	vita2d_pgf_draw_text(pgf, 30, 20, BLACK, 1.0f, "Battle ship arena");
	if (playerOneTurn) vita2d_pgf_draw_text(pgf, 400, 20, GREEN, 1.0f, "Your turn!");
	else vita2d_pgf_draw_text(pgf, 400, 20, RED, 1.0f, "Computer turn!");

	drawField(&playerOneField, BASE_OFFSET_X, BASE_OFFSET_Y, true);

	drawField(&playerTwoField, 480 + BASE_OFFSET_X, BASE_OFFSET_Y, false);
	vita2d_draw_texture_part(items, 480 + BASE_OFFSET_X + cursorX * 30,  BASE_OFFSET_Y + cursorY * 30, 120, 0, 30, 30);

	// Controls help
	vita2d_pgf_draw_text(pgf, 30, 532, BLACK, 1.0f, "[Select] Back to menu  [Arrows] Move cursor  [Cross] Attack cell");
}

// Results ---------------------------------------------------------------------------------
void gameResults()
{
	//Events
	// Back to menu
	if (isJoyButtonPressed(SCE_CTRL_SELECT))
	{
		gameState = GAME_STATE_MENU;
	}	

	// Draw
	if (playerOneWin)
	{
		vita2d_pgf_draw_text(pgf, 440, 250, GREEN, 1.0f, "You win!");
	}
	else
	{
		vita2d_pgf_draw_text(pgf, 440, 250, RED, 1.0f, "You lose...");
	}

	// Statistics
	vita2d_pgf_draw_textf(pgf, 420, 280, BLACK, 1.0f, "destroyed cells: %d", gameStatistic[0]);
	vita2d_pgf_draw_textf(pgf, 420, 310, BLACK, 1.0f, "missed: %d", gameStatistic[1]);

	// Controls help
	vita2d_pgf_draw_text(pgf, 30, 532, BLACK, 1.0f, "[Select] Back to menu");
}

// Main ------------------------------------------------------------------------------------
int main()
{
	if (true) {
		return lite2d::Run<MyApp>(0,NULL);
	}

	initGame();

	SceUInt64 cur_micros = 0, delta_micros = 0, last_micros = 0;

	last_micros = sceKernelGetProcessTimeWide();

	while (!done)
	{
		// Update controls -----------------------------------------------------------------
		oldPad = newPad;
		sceCtrlPeekBufferPositive(0, &newPad, 1);

		ftouched = false;
		sceTouchPeek(SCE_TOUCH_PORT_FRONT, &touch, 1);
        if (touch.reportNum > 0)
        {
            fxTouch = (lerp(touch.report[0].x, 1919, 960));
            fyTouch = (lerp(touch.report[0].y, 1087, 544));
            ftouched = true;
        }

        btouched = false;
        sceTouchPeek(SCE_TOUCH_PORT_BACK, &touch, 1);
        if (touch.reportNum > 0)
        {
            bxTouch = (lerp(touch.report[0].x, 1919, 960));
            byTouch = (lerp(touch.report[0].y, 1087, 544));
            btouched = true;
        }

        // Update time
        cur_micros = sceKernelGetProcessTimeWide();

		// Begin scene ---------------------------------------------------------------------
		vita2d_start_drawing();
		vita2d_clear_screen();

		switch (gameState)
		{
		case GAME_STATE_MENU:
			menu();
			break;

		case GAME_STATE_SUBMENU_DIFFICULTY:
			break;

		case GAME_STATE_CREATE_BASE:
			createBase();
			break;

		case GAME_STATE_BATTLE:
			battle();
			break;

		case GAME_STATE_FINISH:
			gameResults();
			break;

		default:
			break;
		}

		vita2d_pgf_draw_textf(pgf, 840, 20, BLACK, 1.0f, "FPS %.2f", (1.0f / dt));

		// Touch 
		if (ftouched) vita2d_draw_fill_circle(fxTouch, fyTouch, 10, RGBA8(127, 127, 127, 127));

		// End scene -----------------------------------------------------------------------
		vita2d_end_drawing();
		vita2d_swap_buffers();

		delta_micros = cur_micros - last_micros;
		last_micros = cur_micros;
		dt = (float)delta_micros / 1000000.0f;
	}

	termGame();
	sceKernelExitProcess(0);
	return 0;
}
