
#ifndef LITE2D_TEXTURE_H
#define LITE2D_TEXTURE_H

#include "stdio.h"
#include "lite2dObject.h"
#include "string"
#include "map"

namespace lite2d {

class Texture : public Ref {
public:
	Texture(void* texture);
	~Texture();
public:
	int getWidth() const;
	int getHeight() const;
public:
	void* getTexture() const;
private:
	void freeTexture();
private:
	void* texture;
};

class TextureCache {
public:
	typedef std::map<std::string, Texture*> TextureMap;
public:
	TextureCache();
	~TextureCache();
public:
	void registerTexture(std::string const& fileName, unsigned char* buffer, unsigned int bufSize);
	void registerTexture(std::string const& fileName, Texture* tex);
	void removeTexture(std::string const& fileName);
	void removeUnusedTex();
	bool hasTexture(std::string const& fileName);
	Texture* getTexture(std::string const& fileName);
private:
	TextureMap mTexMap;
};

} // lite2d

#endif // LITE2D_TEXTURE_H
