
#ifndef APP_CONTENT_H
#define APP_CONTENT_H

#include "stdio.h"

namespace lite2d {

class HandlerHolder; // notification
class ProxyHolder;   // model
class WorkerHolder;  // controller
class Node;          // view

class DispatcherHolder;
class KeyCtrlHolder;
class TouchHolder;

class TextureCache;

class AppContent {
public:
	static AppContent& instance();
	static void destory();
public:
	HandlerHolder& getHandlerHolder();
	ProxyHolder& getProxyHolder();
	WorkerHolder& getWorkerHolder();
	Node& getRootWindow();
	DispatcherHolder& getDispatcherHolder();
	TouchHolder& getTouchHolder();
	KeyCtrlHolder& getKeyCtrlHolder();
	TextureCache& getTextureCache();
private:
	AppContent();
	~AppContent();
private:
	HandlerHolder* mpHandlerHolder;
	ProxyHolder* mpProxyHolder;
	WorkerHolder* mpWorkerHolder;
	Node* mpRootWindow;
	DispatcherHolder* mpDispatcherHolder;
	TouchHolder* mpTouchHolder;
	KeyCtrlHolder* mpKeyCtrlHolder;
	TextureCache* mpTextureCache;
};

}

#endif // APP_CONTENT_H
