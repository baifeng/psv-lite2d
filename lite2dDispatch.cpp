
#include "lite2dDispatch.h"

namespace lite2d {

void Dispatcher::finish() {
	if (this->mpHolder) {
		this->mpHolder->removeDispatcher(this->mDispatchKey);
	}
}

DispatcherHolder::DispatcherHolder() {

}

DispatcherHolder::~DispatcherHolder() {
	for (DispatcherMap::iterator i = this->mDispatcherMap.begin(); i != this->mDispatcherMap.end(); ++i) {
		delete i->second;
	}
	this->mDispatcherMap.clear();
}

bool DispatcherHolder::registerDispatcher(Dispatcher* target) {
	if (target->getDispatchKey().length()==0) {
		delete target;
		return false;
	}
	Dispatcher* p = this->mDispatcherMap[target->getDispatchKey()];
	if (p != NULL) {
		delete p;
	}
	this->mDispatcherMap[target->getDispatchKey()] = target;
	target->setHolder(this);
	return true;
}

bool DispatcherHolder::removeDispatcher(std::string const& name) {
	Dispatcher* p = this->mDispatcherMap[name];
	this->mDispatcherMap.erase(name);
	if (p != NULL) {
		delete p;
		return true;
	}
	return false;
}

void DispatcherHolder::update(float dt) {
	DispatcherMap temp = this->mDispatcherMap;
	for (DispatcherMap::iterator i = temp.begin(); i != temp.end(); ++i) {
		i->second->update(dt);
	}
}

} // lite2d