
#ifndef LAND_MINE_H
#define LAND_MINE_H

#include "stdio.h"
#include "vector"
#include "string"
#include "lite2dProxy.h"
#include "lite2dStruct.h"

typedef enum {
	TILE_TYPE_NONE = 0,   // 默认
	TILE_TYPE_NUMBER = 1, // 数字
	TILE_TYPE_MINE = 2,   // 地雷
} TileType;

typedef enum {
	TILE_TAG_NONE = 0, // 默认
	TILE_TAG_FLAG = 1, // 旗子
} TileTag;

typedef enum {
	TILE_STATE_CLOSE = 0, // 覆盖
	TILE_STATE_OPEN = 1,  // 打开
	TILE_STATE_SHOW_TIPS = 2, // 提示地雷
} TileState;

typedef struct TileData {
	TileType tileId;   // 空块、雷、或数字
	TileTag tileTag;   // 是否有旗子
	TileState tileState; // 是否打开
	unsigned char tileValue; // 周围的雷数

	TileData() {
		clear();
	}

	void clear() {
		tileId = TILE_TYPE_NONE;
		tileTag = TILE_TAG_NONE;
		tileState = TILE_STATE_CLOSE;
		tileValue = 0;
	}

} TileData;


// 初始化地图逻辑
class TileMapProxy;
class Shuffle {
public:
	typedef struct Tile {
		unsigned char x, y;
		int tileValue;
		TileType tileId;
		Tile() {
			x = y = tileValue = 0;
			tileId = TILE_TYPE_NONE;
		}
	} Tile;
	typedef std::vector<Tile> TileArray;
public:
	Shuffle();
	~Shuffle();
public:
	void input(unsigned char width, unsigned char height, unsigned short number);
	void output(TileMapProxy* proxy);
private:
	TileArray mTileArr;
};


// 翻开空白区域逻辑
class TileMapProxy;
class EmptyTileOpen {
public:
	typedef std::vector<lite2d::Point> TileList;
public:
	EmptyTileOpen();
	~EmptyTileOpen();
public:
	void searchEmptyTile(TileMapProxy* proxy, lite2d::Point const& firstPt);
	void openEmptyTile(TileMapProxy* proxy);
	int getValidOpenCount() const {
		return this->miValidCount;
	}
private:
	int miValidCount;
	TileList mResultList;
};


// 游戏地图
class TileMapProxy : public lite2d::Proxy {
public:
	static std::string const ProxyName;
	static std::string const GameStartEvent;
	static std::string const GameOverEvent;
	static std::string const GameClearEvent;
	static std::string const MineUpdateEvent;
public:
	typedef std::vector<TileData> TileArray;
	typedef std::vector<TileArray> TileDataMap;
	typedef std::vector<lite2d::Point> TileMineArray;
public:
	TileMapProxy(std::string const& name);
	~TileMapProxy();
public:
	TileArray& operator[](unsigned char index);
	TileMineArray& getMineArray();
public:
	// 地图宽、地图高、地雷数量
	bool start(unsigned char width, unsigned char height, unsigned short number);
	void flagTile(int x, int y);
	void openTile(int x, int y);
	void autoOpen(int x, int y);
	bool isValidCoord(int x, int y);
	bool isEmptyTile(int x, int y);
	bool isNumberTile(int x, int y);
	bool isMineTile(int x, int y);
	bool isOpen(int x, int y);
	bool hasFlag(int x, int y);
public:
	int getWidth() const {
		return this->width;
	}
	int getHeight() const {
		return this->height;
	}
	bool isGameOver() const {
		return this->gameover;
	}
	bool isGameClear() const {
		return this->gameclear;
	}
	int getCurMineCount() const {
		return this->miCurMineCount;
	}
	int getCurCloseCount() const {
		return this->miCurCloseCount;
	}
private:
	bool setMapInfo(unsigned char width, unsigned char height, unsigned short number);
	void gameOver();
	void youWin();
	void checkResult();
private:
	bool gameover;
	bool gameclear;
	unsigned char width, height;
	unsigned short mine_count;

	int miCurMineCount; // 当前地雷数
	int miCurCloseCount; // 当前空白数

	TileDataMap mTileMap;
	TileMineArray mMineArray;
};

#endif // LAND_MINE_H
