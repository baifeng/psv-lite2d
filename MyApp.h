
#ifndef MY_APP_H
#define MY_APP_H

#include "lite2dApp.h"

class MyApp : public lite2d::App {
public:
	virtual void onAppStart();
	virtual void onAppStop();
};

#endif // MY_APP_H
