
#ifndef LITE2D_RESPONDER_H
#define LITE2D_RESPONDER_H

#include "stdio.h"
#include "list"

namespace lite2d {

typedef enum {
	APP_CTRL_SELECT     = 0x000001,	//!< Select button.
	APP_CTRL_START      = 0x000008,	//!< Start button.
	APP_CTRL_UP         = 0x000010,	//!< Up D-Pad button.
	APP_CTRL_RIGHT      = 0x000020,	//!< Right D-Pad button.
	APP_CTRL_DOWN       = 0x000040,	//!< Down D-Pad button.
	APP_CTRL_LEFT       = 0x000080,	//!< Left D-Pad button.
	APP_CTRL_LTRIGGER   = 0x000100,	//!< Left trigger.
	APP_CTRL_RTRIGGER   = 0x000200,	//!< Right trigger.
	APP_CTRL_TRIANGLE   = 0x001000,	//!< Triangle button.
	APP_CTRL_CIRCLE     = 0x002000,	//!< Circle button.
	APP_CTRL_CROSS      = 0x004000,	//!< Cross button.
	APP_CTRL_SQUARE     = 0x008000,	//!< Square button.
	APP_CTRL_ANY        = 0x010000	//!< Any input intercepted.
} APP_CTRL_TYPE;

#define APP_CTRL_SIZE_MAX (13)

class TouchResponder {
public:
	friend class TouchHolder;
public:
	virtual ~TouchResponder(){}
public:
	virtual bool is_hit(int local_x, int local_y) const {return false;}
	virtual void convertWorldPoint(int world_x, int world_y, int* local_x, int* local_y) const {}
	virtual void convertLocalPoint(int local_x, int local_y, int* world_x, int* world_y) const {}
protected:
	virtual bool canBeTouch() const {return false;}
	virtual bool canBeHit(int local_x, int local_y) const {return false;}
protected:
	virtual bool onTouchBegin(int local_x, int local_y){return false;}
	virtual void onTouchMoved(int local_x, int local_y){}
	virtual void onTouchEnded(int local_x, int local_y){}
};

class KeyResponder {
public:
	friend class KeyCtrlHolder;
public:
    KeyResponder() {
        this->mKeyState = 0;
    }
	virtual ~KeyResponder(){}
public:
	bool isKeyPressed(APP_CTRL_TYPE key) const {
        return this->mKeyState & key;
    }
protected:
	virtual bool isJoyBtnEnable() const {return false;}
	virtual bool isJoyAxisEnable() const {return false;}
protected:
	virtual void onKeyDown(APP_CTRL_TYPE key){}
	virtual void onKeyUp(APP_CTRL_TYPE key){}
protected:
	virtual void onAxisMoveL(int x, int y){}
	virtual void onAxisMoveR(int x, int y){}
private:
    void setKeyState(APP_CTRL_TYPE key, bool b) {
        if (b) {
            this->mKeyState = this->mKeyState | key;
        }else {
            this->mKeyState = this->mKeyState & (~key);
        }
    }
private:
    int mKeyState;
};

class TouchHolder {
public:
	typedef std::list<TouchResponder*> ResponderList;
public:
	TouchHolder();
	~TouchHolder();
public:
	bool registerResponder(TouchResponder* responder);
	bool removeResponder(TouchResponder* responder);
public:
	bool touchBegin(int world_x, int world_y);
	void touchMoved(int world_x, int world_y);
	void touchEnded(int world_x, int world_y);
private:
	TouchResponder* mpCurResponder;
	ResponderList mResponderlist;
};

class KeyCtrlHolder {
public:
	typedef std::list<KeyResponder*> ResponderList;
public:
	KeyCtrlHolder();
	~KeyCtrlHolder();
public:
	bool registerResponder(KeyResponder* responder);
	bool removeResponder(KeyResponder* responder);
public:
	void keyDown(int key);
	void keyUp(int key);
	void axisMoveL(int x, int y);
	void axisMoveR(int x, int y);
public:
	APP_CTRL_TYPE getKeyState(int index);
private:
	KeyResponder* mpCurResponder;
	ResponderList mResponderlist;
};

} // lite2d

#endif // LITE2D_RESPONDER_H
